import 'package:whatever/model/context_model.dart';

abstract class ContextState {
  const ContextState();
}

class ContextInitialState extends ContextState {}

class ContextLoadInProgress extends ContextState {}

class ContextListLoadSuccess extends ContextState {
  final List<ContextModel> contexts;
  const ContextListLoadSuccess({this.contexts}) : assert(contexts != null);
}

class ContextLoadSuccess extends ContextState {
  final ContextModel contexts;
  const ContextLoadSuccess({this.contexts}) : assert(contexts != null);
}

class ContextsLoadFailure extends ContextState {
  final String error;

  const ContextsLoadFailure({this.error}) : assert(error != null);
}
