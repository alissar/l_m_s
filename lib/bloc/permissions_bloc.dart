import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_event.dart';
import 'package:whatever/bloc/permissions_state.dart';
import '../model/permissions_model.dart';
import '../repositories/permissions_repo.dart';

class PermissionsBloc extends Bloc<PermissionsEvent, PermissionsState> {
  PermissionsRepo _permissionsRepo = PermissionsRepo();

  PermissionsBloc() : super(PermissionsInitial());

  @override
  Stream<PermissionsState> mapEventToState(PermissionsEvent event) async* {
    if (event is AllPermissionsRequested) {
      yield* _mapAllPermissionsRequestedToState(event);
    } else if (event is GetUserPermissions) {
      yield* _mapGetUserPermissionsToState(event);
    } else if (event is AddOrRemovePermissions) {
      yield* _mapAddOrRemovePermissions(event);
    } else if (event is AddPermissionToRole) {
      yield* _mapAddPermissionsToRoleToState(event);
    } else if (event is RemovePermissionsFromRole) {
      yield* _mapPermissionsDeletedToState(event);
    }
  }

  Stream<PermissionsState> _mapPermissionsDeletedToState(
      RemovePermissionsFromRole event) async* {
    yield PermissionsLoadInProgress();
    try {
      await _permissionsRepo.removePermissionFromRole(event.pId, event.id);
      yield PermissionsDeleted(deleted: true);
    } catch (e) {
      yield PermissionsDeleted(deleted: false);
    }
  }

  Stream<PermissionsState> _mapAddPermissionsToRoleToState(
      AddPermissionToRole event) async* {
    yield PermissionsLoadInProgress();
    try {
      await _permissionsRepo.addPermissionsToRole(event.pId, event.id);
      yield PermissionAdded(added: true);
    } catch (e) {
      yield PermissionAdded(added: false);
    }
  }

  Stream<PermissionsState> _mapAddOrRemovePermissions(
      AddOrRemovePermissions event) async* {
    yield PermissionsLoadInProgress();
    try {
      List<PermissionsModel> permissions = await _permissionsRepo
          .addOrRemovePermissions(event.userId, event.permissions);
      yield PermissionsLoadSuccess(permissions: permissions);
    } catch (e) {
      yield PermissionsLoadFailure(error: e.toString());
    }
  }

  Stream<PermissionsState> _mapGetUserPermissionsToState(
      GetUserPermissions event) async* {
    yield PermissionsLoadInProgress();
    try {
      List<PermissionsModel> permissions =
          await _permissionsRepo.getUserPermissions(
        event.userId,
        event.page,
      );
      yield PermissionsLoadSuccess(permissions: permissions);
    } catch (e) {
      yield PermissionsLoadFailure(error: e.toString());
    }
  }

  Stream<PermissionsState> _mapAllPermissionsRequestedToState(
      AllPermissionsRequested event) async* {
    yield PermissionsLoadInProgress();
    try {
      List<PermissionsModel> permissions =
          await _permissionsRepo.getAllPermissions();
      yield PermissionsLoadSuccess(permissions: permissions);
    } catch (e) {
      yield PermissionsLoadFailure(error: e.toString());
    }
  }
}
