abstract class ResetPasswordReqEvent {
  const ResetPasswordReqEvent();
}

class Reset extends ResetPasswordReqEvent {}

class ResetPasswordReq extends ResetPasswordReqEvent {
  final String email;

  ResetPasswordReq({
    this.email,
  });
}
