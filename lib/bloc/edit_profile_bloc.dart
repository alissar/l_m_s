import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/edit_profile_event.dart';
import 'package:whatever/bloc/edit_profile_state.dart';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/repositories/editProfileRepo.dart';
import 'package:whatever/utils/exception.dart';

class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  EditProfileBloc() : super(EditProfileInitial());
  EditProfileRepo _editProfileRepo = EditProfileRepo();

  @override
  Stream<EditProfileState> mapEventToState(EditProfileEvent event) async* {
    if (event is EditProfileRequested) {
      yield* _mapEditProfileRequestedToState(event);
    }
    if (event is Reset) {
      yield* _mapResetToState(event);
    }
  }

  Stream<EditProfileState> _mapEditProfileRequestedToState(
      EditProfileRequested event) async* {
    yield EditProfileInProgress();
    try {
      print('bloc');
      UserModel editProfile = await _editProfileRepo.editProfile(
          event.userId,
          event.firstName,
          event.middleName,
          event.lastName,
          event.nickname,
          event.email,
          event.lang,
          event.theme,
          event.address,
          event.institution,
          event.department,
          event.city,
          event.country,
          event.imagePath,
          event.url);
      print('after repo');
      yield EditProfileLoadSuccess(user: editProfile);
    } on NetworkException catch (e) {
      print(e.message);
      print(e);
      yield EditProfileLoadFailure(error: e.message);
    }
  }

  Stream<EditProfileState> _mapResetToState(Reset event) async* {
    yield EditProfileInitial();
  }
}
