import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/authentication_bloc.dart';
import 'package:whatever/bloc/authentication_event.dart';
import 'package:whatever/bloc/sign_in_event.dart';
import 'package:whatever/bloc/sign_in_state.dart';
import 'package:whatever/repositories/auth_repo.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  final AuthenticationBloc authBloc;
  AuthRepo _authRepo = AuthRepo();

  SignInBloc({@required this.authBloc}) : super(SignInInitial());

  @override
  Stream<SignInState> mapEventToState(SignInEvent event) async* {
    if (event is SignInRequested) {
      yield* _mapSignInRequestedToState(event);
    }
  }

  Stream<SignInState> _mapSignInRequestedToState(SignInRequested event) async* {
    yield SignInInProgress();
    try {
      bool success = await _authRepo.signIn(event.email, event.password);
      if (success) {
        yield SignInSuccess();
        authBloc.add(LoggedIn());
      } else {
        yield SignInFailure(error: 'Could not sign in');
      }
    } catch (e) {
      yield SignInFailure(error: e.toString());
    }
  }
}
