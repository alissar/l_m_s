import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/user_events.dart';
import 'package:whatever/bloc/user_states.dart';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/repositories/user_repo.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserRepo _userRepo = UserRepo();
  UserBloc() : super(UserInitial());

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    if (event is GetAllUsers) {
      yield* _mapGetAllUsersToState(event);
    } else if (event is GetDeletedUsers) {
      yield* _mapGetDeletedUserToState(event);
    } else if (event is DeleteUser) {
      yield* _mapDeleteUserToState(event);
    } else if (event is SearchForUSer) {
      yield* _mapSearchForUsersToState(event);
    } else if (event is UpdateUser) {
      yield* _mapUpdateUserToState(event);
    }
  }

  Stream<UserState> _mapDeleteUserToState(DeleteUser event) async* {
    yield UserLoadInProgress();
    try {
      List<UserModel> users = await _userRepo.deleteUser(event.userId);
      yield UserLoadSuccessList(users: users);
    } catch (e) {
      yield UsersLoadFailure(error: e.toString());
    }
  }

  Stream<UserState> _mapUpdateUserToState(UpdateUser event) async* {
    yield UserLoadInProgress();
    try {
      UserModel user = await _userRepo.updateUser(
        event.userId,
        firstName: event.firstName,
        middleName: event.middleName,
        lastName: event.middleName,
        nickname: event.nickname,
        email: event.email,
        institution: event.institution,
        department: event.department,
        city: event.city,
        country: event.country,
        lang: event.lang,
        picture: event.picture,
        timeCreated: event.timeCreated,
      );
      yield UserLoadSuccess(user: user);
    } catch (e) {
      yield UsersLoadFailure(error: e.toString());
    }
  }

  Stream<UserState> _mapSearchForUsersToState(SearchForUSer event) async* {
    yield UserLoadInProgress();
    try {
      List<UserModel> users = await _userRepo.searchForUser(event.page,
          userId: event.userId,
          email: event.email,
          nickname: event.nickname,
          country: event.country,
          city: event.city,
          institution: event.institution,
          department: event.department,
          firstName: event.firstName,
          middleName: event.middleName,
          lastName: event.lastName,
          lang: event.lang);
      yield UserLoadSuccessList(users: users);
    } catch (e) {
      yield UsersLoadFailure(error: e.toString());
    }
  }

  Stream<UserState> _mapGetAllUsersToState(GetAllUsers event) async* {
    yield UserLoadInProgress();
    try {
      List<UserModel> users = await _userRepo.getAllUsers(
        event.page,
      );
      List<UserModel> deletedUsers =
          await _userRepo.getDeletedUsers(event.page);
      yield UserLoadSuccessList(users: users, deletedUsers: deletedUsers);
    } catch (e) {
      yield UsersLoadFailure(error: e.toString());
    }
  }

  Stream<UserState> _mapGetDeletedUserToState(GetDeletedUsers event) async* {
    yield UserLoadInProgress();
    try {
      List<UserModel> users = await _userRepo.getDeletedUsers(
        event.page,
      );
      yield UserLoadSuccessList(users: users);
    } catch (e) {
      yield UsersLoadFailure(error: e.toString());
    }
  }
}
