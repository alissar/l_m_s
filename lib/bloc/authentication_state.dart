abstract class AuthenticationState {
  const AuthenticationState();
}

class AuthenticationInitial extends AuthenticationState {}

class AuthenticationInProgress extends AuthenticationState {}

class Authenticated extends AuthenticationState {
  final String token;

  const Authenticated({this.token}) : assert(token != null);
}

class Unauthenticated extends AuthenticationState {}
