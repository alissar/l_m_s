import 'package:flutter/material.dart';
import 'package:whatever/model/permissions_model.dart';

abstract class UserAndRolePermissionsCombinedState {
  const UserAndRolePermissionsCombinedState();
}

class UserAndRolePermissionsInitial
    extends UserAndRolePermissionsCombinedState {}

class UserAndRolePermissionsLoadInProgress
    extends UserAndRolePermissionsCombinedState {}

class UserAndRolePermissionsLoadSuccess
    extends UserAndRolePermissionsCombinedState {
  final List<PermissionsModel> permissions;

  const UserAndRolePermissionsLoadSuccess({@required this.permissions})
      : assert(permissions != null);
}

class UserAndRolePermissionsLoadFailure
    extends UserAndRolePermissionsCombinedState {
  final String error;

  const UserAndRolePermissionsLoadFailure({@required this.error})
      : assert(error != null);
}
