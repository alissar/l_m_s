import 'dart:typed_data';

import 'package:flutter/material.dart';

abstract class SignUpEvent {
  const SignUpEvent();
}

class SignUpRequested extends SignUpEvent {
  final String firstName;
  final String middleName;
  final String lastName;
  final String nickname;
  final String email;
  final String password;
  final String phone;
  final String phone2;
  final String address;
  final String institution;
  final String department;
  final String city;
  final String country;
  final Uint8List imagePath;

  const SignUpRequested({
    @required this.firstName,
    @required this.middleName,
    @required this.lastName,
    @required this.nickname,
    @required this.email,
    @required this.password,
    @required this.phone,
    @required this.phone2,
    @required this.address,
    @required this.institution,
    @required this.department,
    @required this.city,
    @required this.country,
    @required this.imagePath,
  })  : assert(firstName != null),
        assert(lastName != null),
        assert(email != null),
        assert(password != null);
}
