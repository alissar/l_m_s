import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/other_profile_event.dart';
import 'package:whatever/bloc/other_profile_state.dart';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/repositories/profile_data_repo.dart';
import 'package:whatever/repositories/user_repo.dart';
import 'package:whatever/utils/exception.dart';

class OtherProfileBloc extends Bloc<OtherProfileEvent, OtherProfileState> {
  OtherProfileBloc() : super(OtherProfileInitial());
  UserRepo _userRepo = UserRepo();
  @override
  Stream<OtherProfileState> mapEventToState(OtherProfileEvent event) async* {
    if (event is OtherProfileRequested) {
      yield* _mapOtherProfileRequestedToState(event);
    }
  }

  Stream<OtherProfileState> _mapOtherProfileRequestedToState(
      OtherProfileRequested event) async* {
    yield OtherProfileLoadInProgress();
    try {
      UserModel _profile = await _userRepo.getUserProfile(event.url);
      yield OtherProfileLoadSuccess(user: _profile);
    } on NetworkException catch (e) {
      yield OtherProfileLoadFailure(error: e.message);
    }
  }
}
