import 'package:whatever/model/user_model.dart';

abstract class UserState {
  const UserState();
}

class UserInitial extends UserState {}

class UserLoadInProgress extends UserState {}

class UserLoadSuccessList extends UserState {
  final List<UserModel> users;
  final List<UserModel> deletedUsers;
  const UserLoadSuccessList({this.deletedUsers, this.users})
      : assert(users != null),
        assert(deletedUsers != null);
}

class UserLoadSuccess extends UserState {
  final UserModel user;
  const UserLoadSuccess({this.user}) : assert(user != null);
}

class UsersLoadFailure extends UserState {
  final String error;
  const UsersLoadFailure({this.error}) : assert(error != null);
}
