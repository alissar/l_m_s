import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/user_permissions_event.dart';
import 'package:whatever/bloc/user_permissions_state.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/repositories/permissions_repo.dart';

class UserPermissionsBloc
    extends Bloc<UserPermissionsEvent, UserPermissionsState> {
  final PermissionsRepo _permissionsRepo = PermissionsRepo();

  UserPermissionsBloc() : super(UserPermissionsStateInitial());

  @override
  Stream<UserPermissionsState> mapEventToState(
      UserPermissionsEvent event) async* {
    if (event is AllPermissionsRequested) {
      yield* _mapAllPermissionsRequestedToState(event);
    }
  }

  Stream<UserPermissionsState> _mapAllPermissionsRequestedToState(
      AllPermissionsRequested event) async* {
    yield UserPermissionsLoanInProgress();
    try {
      List<PermissionsModel> permissions =
          await _permissionsRepo.getAllPermissions();
      yield UserPermissionsLoadSuccess(permissions: permissions);
    } catch (e) {
      yield UserPermissionsLoadFailure(error: e.toString());
    }
  }
}
