abstract class UserEvent {
  const UserEvent();
}

class GetAllUsers extends UserEvent {
  final int page;

  GetAllUsers({
    this.page,
  });
}

class GetDeletedUsers extends UserEvent {
  final int page;

  GetDeletedUsers({this.page});
}

class DeleteUser extends UserEvent {
  final String userId;
  DeleteUser({this.userId});
}

class SearchForUSer extends UserEvent {
  final int page;

  final String userId;
  final String email;
  final String nickname;
  final String country;
  final String city;
  final String institution;
  final String department;
  final String firstName;
  final String middleName;
  final String lastName;
  final String lang;

  SearchForUSer(
      {this.userId,
      this.email,
      this.nickname,
      this.country,
      this.city,
      this.institution,
      this.department,
      this.firstName,
      this.middleName,
      this.lastName,
      this.lang,
      this.page});
}

class UpdateUser extends UserEvent {
  final String userId;
  final String firstName;
  final String middleName;
  final String lastName;
  final String nickname;
  final String email;
  final String institution;
  final String department;
  final String city;
  final String country;
  final String lang;
  final String picture;
  final DateTime timeCreated;

  UpdateUser({
    this.userId,
    this.firstName,
    this.middleName,
    this.lastName,
    this.nickname,
    this.email,
    this.institution,
    this.department,
    this.city,
    this.country,
    this.lang,
    this.picture,
    this.timeCreated,
  });
}
