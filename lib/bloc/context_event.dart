abstract class ContextEvent {
  const ContextEvent();
}

class GetContext extends ContextEvent {}

class PostContext extends ContextEvent {
  final String course;
  final int sourceId;
  PostContext({this.course, this.sourceId});
}
