import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/roles_event.dart';
import 'package:whatever/bloc/roles_state.dart';
import 'package:whatever/model/roles_model.dart';
import 'package:whatever/repositories/roles_repo.dart';

class RolesBloc extends Bloc<RolesEvent, RolesState> {
  final RolesRepo _rolesRepo = RolesRepo();

  RolesBloc() : super(RolesInitial());

  @override
  Stream<RolesState> mapEventToState(RolesEvent event) async* {
    if (event is AllRolesRequested) {
      yield* _mapAllRolesRequestedToState(event);
    }
  }

  Stream<RolesState> _mapAllRolesRequestedToState(
      AllRolesRequested event) async* {
    yield RolesLoadInProgress();
    try {
      List<RolesModel> roles = await _rolesRepo.getAllRoles();
      print(roles);
      yield RolesLoadSuccess(roles: roles);
    } catch (e) {
      print(e);
      yield RolesLoadFailure(error: e.toString());
    }
  }
}
