import 'package:flutter/cupertino.dart';
import 'package:whatever/model/user_model.dart';

abstract class OtherProfileState {
  const OtherProfileState();
}

class OtherProfileInitial extends OtherProfileState {}

class OtherProfileLoadInProgress extends OtherProfileState {}

class OtherProfileLoadSuccess extends OtherProfileState {
  final UserModel user;

  const OtherProfileLoadSuccess({@required this.user}) : assert(user != null);
}

class OtherProfileLoadFailure extends OtherProfileState {
  final String error;
  const OtherProfileLoadFailure({this.error}) : assert(error != null);
}
