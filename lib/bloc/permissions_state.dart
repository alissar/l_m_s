import '../model/permissions_model.dart';

abstract class PermissionsState {
  const PermissionsState();
}

class PermissionsInitial extends PermissionsState {}

class PermissionsLoadInProgress extends PermissionsState {}

class PermissionsLoadSuccess extends PermissionsState {
  final List<PermissionsModel> permissions;

  const PermissionsLoadSuccess({this.permissions})
      : assert(permissions != null);
}

class PermissionAdded extends PermissionsState {
  final bool added;
  const PermissionAdded({this.added}) : assert(added != null);
}

class PermissionsDeleted extends PermissionsState {
  final bool deleted;
  const PermissionsDeleted({this.deleted}) : assert(deleted != null);
}

class PermissionsLoadFailure extends PermissionsState {
  final String error;

  const PermissionsLoadFailure({this.error}) : assert(error != null);
}
