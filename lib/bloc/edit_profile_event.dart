abstract class EditProfileEvent {
  const EditProfileEvent();
}

class EditProfileRequested extends EditProfileEvent {
  final String userId;
  final String firstName;
  final String middleName;
  final String lastName;
  final String nickname;
  final String email;
  final String lang;
  final String theme;
  final String address;
  final String institution;
  final String department;
  final String city;
  final String country;
  final String imagePath;
  final String url;

  EditProfileRequested(
      {this.userId,
      this.firstName,
      this.middleName,
      this.lastName,
      this.nickname,
      this.email,
      this.lang,
      this.theme,
      this.address,
      this.institution,
      this.department,
      this.city,
      this.country,
      this.imagePath,
      this.url});
}

class Reset extends EditProfileEvent {}
