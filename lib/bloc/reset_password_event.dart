abstract class ResetPasswordEvent {
  const ResetPasswordEvent();
}

class Reset extends ResetPasswordEvent {}

class ResetPassword extends ResetPasswordEvent {
  final String newPassword;

  ResetPassword({
    this.newPassword,
  });
}
