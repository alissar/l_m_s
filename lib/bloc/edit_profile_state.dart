import 'package:flutter/cupertino.dart';
import 'package:whatever/model/user_model.dart';

abstract class EditProfileState {
  const EditProfileState();
}

class EditProfileInitial extends EditProfileState {}

class EditProfileInProgress extends EditProfileState {}

class EditProfileLoadSuccess extends EditProfileState {
  final UserModel user;

  const EditProfileLoadSuccess({@required this.user}) : assert(user != null);
}

class EditProfileLoadFailure extends EditProfileState {
  final String error;
  const EditProfileLoadFailure({this.error}) : assert(error != null);
}
