import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/user_and_role_permissions_combined_event.dart';
import 'package:whatever/bloc/user_and_role_permissions_combined_state.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/repositories/permissions_repo.dart';

class UserAndRolePermissionsCombinedBloc extends Bloc<
    UserAndRolePermissionsCombinedEvent, UserAndRolePermissionsCombinedState> {
  final PermissionsRepo _permissionsRepo = PermissionsRepo();
  UserAndRolePermissionsCombinedBloc() : super(UserAndRolePermissionsInitial());

  @override
  Stream<UserAndRolePermissionsCombinedState> mapEventToState(
      UserAndRolePermissionsCombinedEvent event) async* {
    if (event is UserPermissionsCombinedWithRolePermissionsRequested) {
      yield* _mapUserPermissionsCombinedWithRolePermissionsRequestedToState(
          event);
    }
  }

  Stream<UserAndRolePermissionsCombinedState>
      _mapUserPermissionsCombinedWithRolePermissionsRequestedToState(
          UserPermissionsCombinedWithRolePermissionsRequested event) async* {
    yield UserAndRolePermissionsLoadInProgress();
    try {
      // TODO ask Ayham on context
      List<PermissionsModel> permissions = await _permissionsRepo
          .getUserPermissionsCombinesWithHisRolePermissions(
              event.context, event.userUrl);
      yield UserAndRolePermissionsLoadSuccess(permissions: permissions);
    } catch (e) {
      yield UserAndRolePermissionsLoadFailure(error: e.toString());
    }
  }
}
