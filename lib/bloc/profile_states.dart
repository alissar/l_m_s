import 'package:flutter/material.dart';
import 'package:whatever/model/user_model.dart';

abstract class ProfileState {
  const ProfileState();
}

class ProfileInitial extends ProfileState {}

class ProfileLoadInProgress extends ProfileState {}

class ProfileLoadSuccess extends ProfileState {
  final UserModel user;

  const ProfileLoadSuccess({@required this.user}) : assert(user != null);
}

class ProfileLoadFailure extends ProfileState {
  final String error;
  const ProfileLoadFailure({this.error}) : assert(error != null);
}
