import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/context_event.dart';
import 'package:whatever/bloc/context_state.dart';
import 'package:whatever/model/context_model.dart';
import 'package:whatever/repositories/context_repo.dart';

class ContextBloc extends Bloc<ContextEvent, ContextState> {
  ContextRepo _contextRepo = ContextRepo();
  ContextBloc() : super(ContextInitialState());

  @override
  Stream<ContextState> mapEventToState(ContextEvent event) async* {
    if (event is GetContext) {
      yield* _mapGetContextToState(event);
    } else if (event is PostContext) {
      yield* _mapPostContextToState(event);
    }
  }

  Stream<ContextState> _mapGetContextToState(GetContext event) async* {
    yield ContextLoadInProgress();
    try {
      List<ContextModel> contexts = await _contextRepo.getContext();
      yield ContextListLoadSuccess(contexts: contexts);
    } catch (e) {
      yield ContextsLoadFailure();
    }
  }

  Stream<ContextState> _mapPostContextToState(PostContext event) async* {
    yield ContextLoadInProgress();
    try {
      await _contextRepo.postContext(event.course, event.sourceId);
      yield ContextLoadSuccess();
    } catch (e) {
      yield ContextsLoadFailure();
    }
  }
}
