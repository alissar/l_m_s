import 'package:flutter/material.dart';

abstract class SignInEvent {
  const SignInEvent();
}

class SignInRequested extends SignInEvent {
  final String email;
  final String password;

  const SignInRequested({@required this.email, @required this.password})
      : assert(email != null),
        assert(password != null);
}
