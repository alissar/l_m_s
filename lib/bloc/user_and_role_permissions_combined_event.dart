import 'package:flutter/material.dart';

abstract class UserAndRolePermissionsCombinedEvent {
  const UserAndRolePermissionsCombinedEvent();
}

class UserPermissionsCombinedWithRolePermissionsRequested
    extends UserAndRolePermissionsCombinedEvent {
  final String userUrl;
  final String context;

  const UserPermissionsCombinedWithRolePermissionsRequested({
    @required this.userUrl,
    @required this.context,
  })  : assert(userUrl != null),
        assert(context != null);
}
