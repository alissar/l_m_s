abstract class ResetPasswordState {
  const ResetPasswordState();
}

class ResetPasswordInitialState extends ResetPasswordState {}

class ResetPasswordInProgress extends ResetPasswordState {}

class ResetPasswordLoadSuccess extends ResetPasswordState {}

class ResetPasswordLoadOnFailure extends ResetPasswordState {
  final String error;
  const ResetPasswordLoadOnFailure({this.error}) : assert(error != null);
}
