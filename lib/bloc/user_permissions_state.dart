import 'package:whatever/model/permissions_model.dart';

abstract class UserPermissionsState {
  const UserPermissionsState();
}

class UserPermissionsStateInitial extends UserPermissionsState {}

class UserPermissionsLoanInProgress extends UserPermissionsState {}

class UserPermissionsLoadSuccess extends UserPermissionsState {
  final List<PermissionsModel> permissions;

  const UserPermissionsLoadSuccess({this.permissions})
      : assert(permissions != null);
}

class UserPermissionsLoadFailure extends UserPermissionsState {
  final String error;

  const UserPermissionsLoadFailure({this.error}) : assert(error != null);
}
