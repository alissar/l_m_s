import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/model/roles_model.dart';

abstract class RoleState {
  const RoleState();
}

class RoleInitialState extends RoleState {}

class RoleLoadInProgress extends RoleState {}

class RoleListLoadSuccess extends RoleState {
  final List<RolesModel> roles;

  const RoleListLoadSuccess({this.roles}) : assert(roles != null);
}

class RoleLoadSuccess extends RoleState {
  final RolesModel roles;

  const RoleLoadSuccess({this.roles}) : assert(roles != null);
}

class RolesLoadFailure extends RoleState {
  final String error;

  const RolesLoadFailure({this.error}) : assert(error != null);
}

class RolePermissionsLoadSuccess extends RoleState {
  final List<PermissionsModel> permissions;

  RolePermissionsLoadSuccess({this.permissions}) : assert(permissions != null);
}
