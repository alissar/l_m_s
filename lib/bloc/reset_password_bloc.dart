import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/reset_password_event.dart';
import 'package:whatever/bloc/reset_password_state.dart';
import 'package:whatever/common/user_preferences.dart';
import 'package:whatever/repositories/auth_repo.dart';

class ResetPasswordBloc extends Bloc<ResetPasswordEvent, ResetPasswordState> {
  final AuthRepo _authRepo = AuthRepo();

  ResetPasswordBloc() : super(ResetPasswordInitialState());

  @override
  Stream<ResetPasswordState> mapEventToState(ResetPasswordEvent event) async* {
    if (event is ResetPassword) {
      yield* _mapResetPasswordToState(event);
    }
    if (event is Reset) {
      yield ResetPasswordInitialState();
    }
  }

  Stream<ResetPasswordState> _mapResetPasswordToState(
      ResetPassword event) async* {
    yield ResetPasswordInProgress();
    try {
      String token = await UserPreferences().getToken();
      await _authRepo.resetPassword(event.newPassword, token);
      yield ResetPasswordLoadSuccess();
    } catch (e) {
      yield ResetPasswordLoadOnFailure(error: e.toString());
    }
  }
}
