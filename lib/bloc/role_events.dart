abstract class RoleEvents {
  const RoleEvents();
}

class GetRole extends RoleEvents {
  final String id;
  final String context;

  GetRole({this.id, this.context});
}

class AssignRole extends RoleEvents {
  final double role;
  final String id;
  final double context;
  final dDay;
  AssignRole({
    this.role,
    this.id,
    this.context,
    this.dDay,
  });
}

class CreateNewRole extends RoleEvents {
  final String name;
  final String description;
  final String order;
  final List<int> permissions;

  CreateNewRole({this.name, this.description, this.order, this.permissions});
}

class GetRolePermissions extends RoleEvents {
  final int roleId;
  final int page;

  GetRolePermissions({this.roleId, this.page});
}
