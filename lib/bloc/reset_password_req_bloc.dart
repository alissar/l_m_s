import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/reset_password_req_event.dart';
import 'package:whatever/bloc/reset_password_req_state.dart';
import 'package:whatever/repositories/reset_password_req_repo.dart';

class ResetPasswordReqBloc
    extends Bloc<ResetPasswordReqEvent, ResetPasswordReqState> {
  ResetPasswordReqBloc() : super(ResetPasswordReqinitialState());

  @override
  Stream<ResetPasswordReqState> mapEventToState(
      ResetPasswordReqEvent event) async* {
    if (event is ResetPasswordReq) {
      yield* _mapResetPasswordReqToState(event);
    }
    if (event is Reset) {
      yield ResetPasswordReqinitialState();
    }
  }

  Stream<ResetPasswordReqState> _mapResetPasswordReqToState(
      ResetPasswordReq event) async* {
    yield ResetPasswordReqLoadInProgress();
    try {
      ResetPasswordReqRepo _resetPasswordReqRepo = ResetPasswordReqRepo();
      await _resetPasswordReqRepo.getResetPasswordRequest(event.email);

      yield ResetPasswordReqLoadSuccess();
    } catch (e) {
      yield ResetPasswordReqLoadInFailure(error: e.toString());
    }
  }
}
