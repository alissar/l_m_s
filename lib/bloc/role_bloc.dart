import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/role_events.dart';
import 'package:whatever/bloc/role_states.dart';
import 'package:whatever/bloc/roles_event.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/model/roles_model.dart';
import 'package:whatever/repositories/roles_repo.dart';

class RoleBloc extends Bloc<RoleEvents, RoleState> {
  RolesRepo _rolesRepo = RolesRepo();
  RoleBloc() : super(RoleInitialState());

  @override
  Stream<RoleState> mapEventToState(RoleEvents event) async* {
    if (event is GetRole) {
      yield* _mapGetRoleEventToState(event);
    } else if (event is AssignRole) {
      yield* _mapAssignRoleToState(event);
    } else if (event is CreateNewRole) {
      yield* _mapCreateNewRoleToState(event);
    } else if (event is GetRolePermissions) {
      yield* _mapGetRolePermissionsToState(event);
    }
  }

  Stream<RoleState> _mapGetRoleEventToState(GetRole event) async* {
    yield RoleLoadInProgress();
    try {
      List<RolesModel> roles =
          await _rolesRepo.getRole(event.id, event.context);
      yield RoleListLoadSuccess(roles: roles);
    } catch (e) {
      yield RolesLoadFailure(error: e.toString());
    }
  }

  Stream<RoleState> _mapAssignRoleToState(AssignRole event) async* {
    yield RoleLoadInProgress();
    try {
      // List<RolesModel> roles = await _rolesRepo.assignRoleTo(
      //     event.role, event.id, event.context, event.dDay);
      // yield RoleLoadSuccess(roles: roles);
    } catch (e) {
      yield RolesLoadFailure(error: e.toString());
    }
  }

  Stream<RoleState> _mapCreateNewRoleToState(CreateNewRole event) async* {
    yield RoleLoadInProgress();
    try {
      RolesModel roles = await _rolesRepo.createNewRole(
          event.name, event.description, event.order, event.permissions);
      yield RoleLoadSuccess(roles: roles);
      throw UnimplementedError();
    } catch (e) {
      yield RolesLoadFailure(error: e.toString());
    }
  }

  Stream<RoleState> _mapGetRolePermissionsToState(
      GetRolePermissions event) async* {
    yield RoleLoadInProgress();
    try {
      List<PermissionsModel> permissions =
          await _rolesRepo.getRolePermissions(event.roleId, event.page);
      yield RolePermissionsLoadSuccess(permissions: permissions);
    } catch (e) {
      yield RolesLoadFailure(error: e.toString());
    }
  }
}
