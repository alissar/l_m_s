import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/profile_events.dart';
import 'package:whatever/bloc/profile_states.dart';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/repositories/profile_data_repo.dart';
import 'package:whatever/utils/exception.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(ProfileInitial());
  ProfileRepoData _profileRepoData = ProfileRepoData();
  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is ProfileRequested) {
      yield* _mapProfileRequestedToState(event);
    }
  }

  Stream<ProfileState> _mapProfileRequestedToState(
      ProfileRequested event) async* {
    yield ProfileLoadInProgress();
    try {
      UserModel _profile = await _profileRepoData.getProfileInfo();
      yield ProfileLoadSuccess(user: _profile);
    } on NetworkException catch (e) {
      yield ProfileLoadFailure(error: e.message);
    }
  }
}
