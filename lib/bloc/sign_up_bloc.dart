import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/sign_up_event.dart';
import 'package:whatever/bloc/sign_up_state.dart';
import 'package:whatever/repositories/auth_repo.dart';
import 'package:whatever/repositories/email_validate_repo.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  AuthRepo _authRepo = AuthRepo();
  EmailValidateRepo _emailValidateRepo = EmailValidateRepo();

  SignUpBloc() : super(SignUpInitial());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is SignUpRequested) {
      yield* _mapSignUpRequestedToState(event);
    }
  }

  Stream<SignUpState> _mapSignUpRequestedToState(SignUpRequested event) async* {
    yield SignUpInProgress();
    try {
      bool success = await _authRepo.signUp(
          event.firstName,
          event.middleName,
          event.lastName,
          event.nickname,
          event.email,
          event.password,
          event.phone,
          event.phone2,
          event.address,
          event.institution,
          event.department,
          event.city,
          event.country,
          event.imagePath);
      if (success) {
        _emailValidateRepo.validateEmail();
        yield SignUpSuccess();
      } else {
        yield SignUpFailure(error: 'Could not sign up');
      }
    } catch (e) {
      yield SignUpFailure(error: e.toString());
    }
  }
}
