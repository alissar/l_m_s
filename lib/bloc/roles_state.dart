import 'package:flutter/material.dart';
import 'package:whatever/model/roles_model.dart';

abstract class RolesState {
  const RolesState();
}

class RolesInitial extends RolesState {}

class RolesLoadInProgress extends RolesState {}

class RolesLoadSuccess extends RolesState {
  final List<RolesModel> roles;

  RolesLoadSuccess({@required this.roles}) : assert(roles != null);
}

class RolesLoadFailure extends RolesState {
  final String error;

  RolesLoadFailure({@required this.error}) : assert(error != null);
}
