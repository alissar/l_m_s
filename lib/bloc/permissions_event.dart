abstract class PermissionsEvent {
  const PermissionsEvent();
}

class AllPermissionsRequested extends PermissionsEvent {}

class GetUserPermissions extends PermissionsEvent {
  final String userId;
  final int page;
  final int size;

  GetUserPermissions({this.userId, this.page, this.size});
}

class AddOrRemovePermissions extends PermissionsEvent {
  final String userId;
  final List<int> permissions;

  AddOrRemovePermissions({this.userId, this.permissions});
}

class AddPermissionToRole extends PermissionsEvent {
  final List<int> pId;
  final String id;

  AddPermissionToRole({
    this.pId,
    this.id,
  });
}

class RemovePermissionsFromRole extends PermissionsEvent {
  final List<int> pId;
  final int id;

  RemovePermissionsFromRole({this.pId, this.id});
}
