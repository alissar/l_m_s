abstract class ResetPasswordReqState {
  const ResetPasswordReqState();
}

class ResetPasswordReqinitialState extends ResetPasswordReqState {}

class ResetPasswordReqLoadInProgress extends ResetPasswordReqState {}

class ResetPasswordReqLoadSuccess extends ResetPasswordReqState {}

class ResetPasswordReqLoadInFailure extends ResetPasswordReqState {
  final String error;

  const ResetPasswordReqLoadInFailure({this.error}) : assert(error != null);
}
