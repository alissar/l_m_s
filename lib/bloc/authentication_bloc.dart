import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/authentication_event.dart';
import 'package:whatever/bloc/authentication_state.dart';
import 'package:whatever/common/user_preferences.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(Unauthenticated());

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState(event);
    }
    if (event is LoggedIn) {
      yield* _mapLoggedInToState(event);
    }
    if (event is LoggedOut) {
      yield* _mapLoggedOutToState(event);
    }
  }

  Stream<AuthenticationState> _mapAppStartedToState(AppStarted event) async* {
    yield Unauthenticated();
  }

  Stream<AuthenticationState> _mapLoggedInToState(LoggedIn event) async* {
    yield AuthenticationInProgress();
    try {
      String token = await UserPreferences().getToken();
      yield Authenticated(token: token);
    } catch (e) {
      yield Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedOutToState(LoggedOut event) async* {
    yield Unauthenticated();
  }
}
