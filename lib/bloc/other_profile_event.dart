import 'package:flutter/cupertino.dart';

abstract class OtherProfileEvent {
  const OtherProfileEvent();
}

class OtherProfileRequested extends OtherProfileEvent {
  @required
  final String url;
  OtherProfileRequested({this.url});
}
