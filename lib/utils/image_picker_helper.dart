import 'dart:typed_data';
import 'package:image_picker/image_picker.dart';

class ImagePickerHelper {
  static final ImagePicker _imagePicker = ImagePicker();

  static Future<String> getImagePath() async {
    final pickedFile = await _imagePicker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      return pickedFile.path.substring(pickedFile.path.indexOf(':') + 1);
    }

    return null;
  }

  static Future<Uint8List> getImageAsBytes() async {
    final pickedFile = await _imagePicker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      return pickedFile.readAsBytes();
    }

    return null;
  }

  static Future<String> getImageBytesAsString() async {
    Uint8List bytes = await getImageAsBytes();
    if (bytes != null) {
      return bytes.toString();
    }

    return null;
  }
}
