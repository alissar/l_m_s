import 'package:whatever/service/reset_password_service.dart';

class ResetPasswordRepo {
  ResetPasswordService _resetpasswordService = ResetPasswordService();

  ///TODO check with Bob
  Future<void> resetPassword(String password, String token) async {
    try {
      await _resetpasswordService.resetPassword(password, token);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
