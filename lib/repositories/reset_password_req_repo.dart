import 'package:whatever/service/reset_password_request_service.dart';

class ResetPasswordReqRepo {
  ResetPasswordRequestService _resetPasswordRequestService =
      ResetPasswordRequestService();

  Future<void> getResetPasswordRequest(String email) async {
    try {
      await _resetPasswordRequestService.resetPasswordRequest(email);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
