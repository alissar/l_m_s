import 'dart:convert';

import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/model/roles_model.dart';
import 'package:whatever/service/roles_service.dart';

class RolesRepo {
  RolesService _rolesService = RolesService();
  static const int pageSize = 25;

  Future<RolesModel> createNewRole(String name, String description,
      String order, List<int> permissions) async {
    dynamic jsonRoles = jsonDecode(await _rolesService.createNewRole(
        name, description, order, permissions));
    RolesModel createNewRole = RolesModel.fromJson(jsonRoles);
    return createNewRole;
  }

  Future<RolesModel> assignRoleTo(
      double role, String id, double context, dDay) async {
    dynamic jsonRoles =
        jsonDecode(await _rolesService.assignRoleTo(role, id, context, dDay));
    RolesModel assignRoleTo = RolesModel.fromJson(jsonRoles);
    return assignRoleTo;
  }

  Future<List<RolesModel>> getRole(
    String id,
    String context,
  ) async {
    List<dynamic> jsonRoles =
        jsonDecode(await _rolesService.getRole(id, context)) as List;
    List<RolesModel> getRole =
        jsonRoles.map((e) => RolesModel.fromJson(e)).toList();

    return getRole;
  }

  Future<List<RolesModel>> getAllRoles() async {
    List<dynamic> jsonRoles =
        jsonDecode(await _rolesService.getAllRoles()) as List;
    print(jsonRoles);
    List<RolesModel> roles =
        jsonRoles.map((e) => RolesModel.fromJson(e)).toList();
    return roles;
  }

  Future<List<PermissionsModel>> getRolePermissions(
      int roleId, int page) async {
    List<dynamic> jsonPermissions = jsonDecode(
        await _rolesService.getRolePermissions(roleId, page, pageSize)) as List;

    List<PermissionsModel> userPermissions =
        jsonPermissions.map((e) => PermissionsModel.fromJson(e)).toList();

    return userPermissions;
  }
}
