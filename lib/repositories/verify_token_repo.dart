import 'package:whatever/service/verify_token_service.dart';
import 'package:whatever/utils/exception.dart';

class VerifyTokenRepo {
  VerifyTokenService _verifyTokenService = VerifyTokenService();

  Future<bool> verifyToken(String token) async {
    try {
      await _verifyTokenService.verifyToken(token);
      return true;
    } on NetworkException catch (e) {
      if (e.message == '401') {
        return false;
      } else {
        throw e;
      }
    }
  }
}
