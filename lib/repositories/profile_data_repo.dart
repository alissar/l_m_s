import 'dart:convert';

import 'package:whatever/model/user_model.dart';
import 'package:whatever/service/profile_data_service.dart';
import 'package:whatever/utils/exception.dart';

class ProfileRepoData {
  ProfileDataService _profileDataService = ProfileDataService();

  Future<UserModel> getProfileInfo() async {
    Map<String, dynamic> jsonProfile =
        jsonDecode(await _profileDataService.getuserProfileInfo());

    UserModel profileInfo = UserModel.fromJson(jsonProfile);
    return profileInfo;
  }
}
