import 'dart:convert';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/service/edit_profile.dart';

class EditProfileRepo {
  EditProfileService _editProfileService = EditProfileService();

  Future<UserModel> editProfile(
      String userId,
      String firstName,
      String middleName,
      String lastName,
      String nickname,
      String email,
      String lang,
      String theme,
      String address,
      String institution,
      String department,
      String city,
      String country,
      String imagePath,
      String url) async {
    Map<String, dynamic> jsonProfile = jsonDecode(
        await _editProfileService.editProfileInfo(
            userId,
            firstName,
            middleName,
            lastName,
            nickname,
            email,
            lang,
            theme,
            address,
            institution,
            department,
            city,
            country,
            imagePath,
            url));
    UserModel editProfileInfo = UserModel.fromJson(jsonProfile);
    return editProfileInfo;
  }
}
