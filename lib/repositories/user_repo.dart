import 'dart:convert';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/service/users_service.dart';

class UserRepo {
  static const pageSize = 25;
  UsersService _usersService = UsersService();

  Future<List<UserModel>> getAllUsers(
    int page,
  ) async {
    List<dynamic> jsonUsers =
        jsonDecode(await _usersService.getAllUsers(page, pageSize)) as List;
    List<UserModel> getAllUsers =
        jsonUsers.map((e) => UserModel.fromJson(e)).toList();

    return getAllUsers;
  }

  Future<List<UserModel>> getDeletedUsers(
    int page,
  ) async {
    List<dynamic> jsonUsers =
        jsonDecode(await _usersService.getDeletedUsers(page, pageSize)) as List;
    List<UserModel> getDeletedUsers =
        jsonUsers.map((e) => UserModel.fromJson(e)).toList();
    return getDeletedUsers;
  }

  Future<UserModel> updateUser(String userId,
      {String firstName,
      String middleName,
      String lastName,
      String nickname,
      String email,
      String institution,
      String department,
      String city,
      String country,
      String lang,
      String picture,
      DateTime timeCreated}) async {
    dynamic jsonUsers = jsonDecode(await _usersService.updateUser(userId));
    UserModel updateUser = UserModel.fromJson(jsonUsers);
    return updateUser;
  }

  Future<List<UserModel>> deleteUser(String userId) async {
    List<dynamic> jsonUsers =
        jsonDecode(await _usersService.deleteUser(userId)) as List;
    List<UserModel> deleteUser =
        jsonUsers.map((e) => UserModel.fromJson(e)).toList();
    return deleteUser;
  }

  Future<List<UserModel>> searchForUser(int page,
      {String userId,
      String email,
      String nickname,
      String country,
      String city,
      String institution,
      String department,
      String firstName,
      String middleName,
      String lastName,
      String lang}) async {
    List<dynamic> jsonUsers =
        jsonDecode(await _usersService.searchForUser(page, pageSize)) as List;
    List<UserModel> searchForUser =
        jsonUsers.map((e) => UserModel.fromJson(e)).toList();
    return searchForUser;
  }

  Future<UserModel> getUserProfile(String userUrl) async {
    dynamic jsonUser = jsonDecode(await _usersService.getUserProfile(userUrl));
    UserModel getAllUsers = UserModel.fromJson(jsonUser);

    return getAllUsers;
  }
}
