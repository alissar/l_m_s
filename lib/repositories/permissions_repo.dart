import 'dart:convert';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/service/permissions_service.dart';

class PermissionsRepo {
  PermissionsService _permissionsService = PermissionsService();
  static const pageSize = 25;

  Future<List<PermissionsModel>> getAllPermissions() async {
    List<dynamic> jsonPermissions =
        jsonDecode(await _permissionsService.getAllPermissions()) as List;

    List<PermissionsModel> permissions =
        jsonPermissions.map((e) => PermissionsModel.fromJson(e)).toList();

    return permissions;
  }

  Future<List<PermissionsModel>> getUserPermissions(
      String userId, int page) async {
    List<dynamic> jsonPermissions = jsonDecode(await _permissionsService
        .getUserPermissions(userId, page, pageSize)) as List;

    List<PermissionsModel> userPermissions =
        jsonPermissions.map((e) => PermissionsModel.fromJson(e)).toList();

    return userPermissions;
  }

  Future<List<PermissionsModel>>
      getUserPermissionsCombinesWithHisRolePermissions(
          String context, String userUrl) async {
    List<dynamic> jsonPermissions = jsonDecode(await _permissionsService
            .getUserPermissionsCombinesWithHisRolePermissions(context, userUrl))
        as List;

    List<PermissionsModel> permissions =
        jsonPermissions.map((e) => PermissionsModel.fromJson(e)).toList();

    return permissions;
  }

  Future<List<PermissionsModel>> addOrRemovePermissions(
      String userId, List<int> permissions) async {
    List<Map<String, dynamic>> servicePermissions = [];

    // TODO check back to see what granted means and where to get context from
    for (int permission in permissions) {
      servicePermissions.add({
        'userID': userId,
        'permission': permission,
        'granted': true,
        'context': '',
      });
    }

    List<dynamic> jsonPermissions = jsonDecode(await _permissionsService
        .addOrRemovePermissions(userId, servicePermissions)) as List;

    List<PermissionsModel> addOrRemovePermissions =
        jsonPermissions.map((e) => PermissionsModel.fromJson(e)).toList();

    return addOrRemovePermissions;
  }

  Future<void> addPermissionsToRole(List<int> pId, String id) async {
    await _permissionsService.addPermissionToRole(pId, id);
  }

  Future<void> removePermissionFromRole(
    List<int> pId,
    int id,
  ) async {
    await _permissionsService.deletePermissionFromRole(pId, id);
  }
}
