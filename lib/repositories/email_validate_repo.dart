import 'package:whatever/service/email_validate_service.dart';

class EmailValidateRepo {
  EmailValidateService _emailValidateService = EmailValidateService();

  Future<void> validateEmail() async {
    await _emailValidateService.validateEmail();
  }
}
