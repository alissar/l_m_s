import 'dart:convert';
import 'package:whatever/model/context_model.dart';
import 'package:whatever/service/context_service.dart';

class ContextRepo {
  ContextService _contextService = ContextService();

  Future<List<ContextModel>> getContext() async {
    List<dynamic> jsonContext =
        jsonDecode(await _contextService.getContext()) as List;

    List<ContextModel> context =
        jsonContext.map((e) => ContextModel.fromJson(e)).toList();
    return context;
  }

  Future<void> postContext(String course, int sourceId) async {
    await _contextService.postContext(course, sourceId);
  }
}
