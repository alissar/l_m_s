import 'dart:typed_data';

import 'package:whatever/service/auth_service.dart';

class AuthRepo {
  final AuthService _authService = AuthService();

  Future<bool> signIn(String email, String password) async {
    return await _authService.signIn(email, password);
  }

  Future<bool> signUp(
    String firstName,
    String middleName,
    String lastName,
    String nickname,
    String email,
    String password,
    String phone,
    String phone2,
    String address,
    String institution,
    String department,
    String city,
    String country,
    Uint8List imagePath,
  ) async {
    return await _authService.signUp(
        firstName,
        middleName,
        lastName,
        nickname,
        email,
        password,
        phone,
        phone2,
        address,
        institution,
        department,
        city,
        country,
        imagePath);
  }

  Future<void> resetPassword(String newPassword, String token) async {
    return await _authService.resetPassword(newPassword, token);
  }
}
