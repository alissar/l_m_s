import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:whatever/service/service.dart';
import 'package:whatever/utils/exception.dart';

class ResetPasswordService extends Service {
  static final String _url =
      '8a025d62-3e76-4f61-b03b-aed03b0fb979.mock.pstmn.io';

  Future<String> resetPassword(String password, String token) async {
    Map<String, dynamic> queryParams = {
      'password': password,
      'token': token,
    };
    http.Response response = await bc.post(
        Uri.https(_url, 'reset-password', queryParams),
        headers: <String, String>{
          'Content-type': 'application/json',
        });
    return _processResponse(response);
  }

  Future<String> _processResponse(http.Response response) async {
    print(response.body);
    switch (response.statusCode) {
      case 200:
        {
          return response.body;
        }
      case 400:
        {
          List errors = jsonDecode(response.body);
          String message = errors[errors.length - 1];

          throw NetworkException(message: message, logMessage: '400');
        }
      case 401:
        {
          throw NetworkException(message: '401');
        }
      case 403:
        {
          throw NetworkException(message: '403');
        }
      case 404:
        {
          throw NetworkException(message: '404');
        }
      case 408:
        {
          throw NetworkException(message: '408');
        }
      case 409:
        {
          throw DatabaseException(message: '409');
        }
      default:
        {
          throw NetworkException(message: "Unknown Error");
        }
    }
  }
}
