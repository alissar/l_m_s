import 'dart:convert';
import 'dart:typed_data';
import 'package:http/browser_client.dart';
import 'package:whatever/common/user_preferences.dart';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/service/service.dart';
import 'package:http/http.dart' as http;
import 'package:whatever/utils/exception.dart';

class AuthService extends Service {
  Future<bool> signIn(String email, String password) async {
    // POST method with email and password as body
    http.Response response = await bc.post(
      Uri.http(url, 'login'),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
      }),
    );
    print(response.body);
    // success
    if (response.statusCode == 200) {
      response =
          await bc.post(Uri.http(url, 'Token'), headers: <String, String>{
        'Content-Type': 'application/json',
      });

      if (response.statusCode == 200) {
        // get token from response for debugging purposes only4
        print("200");
        String token = response.body;
        print(token);
        await UserPreferences().saveUserToken(token);
        return true;
      }
    }

    return false;
  }

  Future<bool> signUp(
    String firstName,
    String middleName,
    String lastName,
    String nickname,
    String email,
    String password,
    String phone,
    String phone2,
    String address,
    String institution,
    String department,
    String city,
    String country,
    Uint8List imagePath,
  ) async {
    var request = http.MultipartRequest(
      'POST',
      Uri.http(url, '/SignUp'),
    );
    var request1 = http.MultipartRequest(
      'POST',
      Uri.http(url, '/SignUp'),
    );

    if (imagePath != null) {
      request.files.add(
        http.MultipartFile.fromBytes(
          'file',
          imagePath,
          filename: 'image.jpg',
        ),
      );
      request1.files.add(
        http.MultipartFile.fromBytes(
          'file',
          imagePath,
          filename: 'image.jpg',
        ),
      );
    }
    var infos = jsonEncode({
      'firstName': firstName,
      'fatherName': middleName,
      'lastName': lastName,
      'email': email,
      'unEncryptedPassword': password,
      "authentication": "Default",
      "lang": "English",
      "theme": "Default"
    });
    request.fields.addAll({'info': infos});
    request1.fields.addAll({'info': infos});

    http.StreamedResponse streamedResponse = await bc.send(request);

    streamedResponse = await bc.send(request1);

    http.Response response = await http.Response.fromStream(streamedResponse);

    if (response.statusCode == 200) {
      UserModel user = UserModel.fromJson(jsonDecode(response.body));
      UserPreferences().saveUser(user);

      return true;
    }

    return false;
  }

  Future<void> resetPassword(String newPassword, String token) async {
    http.Response response = await bc.post(
      Uri.http(url, '/reset-password/'),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body: jsonEncode(<String, String>{
        'password': newPassword,
        'token':
            token, // THIS MUST BE THE TOKEN YOU GET FROM BACKEND AND NOT A RANDOM ONE
      }),
    );

    if (response.statusCode != 200) {
      throw NetworkException(message: response.statusCode.toString());
    }
  }
}
