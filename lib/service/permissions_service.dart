import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:whatever/service/service.dart';
import 'package:whatever/utils/exception.dart';

class PermissionsService extends Service {
  var dDay = DateTime.utc(1944, 6, 6);

  Future<String> getUserPermissions(String userId, int page, int size) async {
    http.Response response = await bc.get(
        Uri.http(
          url,
          '/users/permissions/$userId/$page/$size/',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json',
        });
    return _processResponse(response);
  }

  Future<String> getUserPermissionsCombinesWithHisRolePermissions(
      String context, String userUrl) async {
    http.Response response = await bc.get(
        Uri.http(url, '/$context/users/$userUrl/permissions/'),
        headers: <String, String>{
          'Content-Type': 'appliction/json',
        });

    return _processResponse(response);
  }

  Future<String> addPermissionToRole(List<int> pId, String id) async {
    List<Map<String, dynamic>> ids = [];
    for (int id in pId) {
      ids.add({'id': id.toString()});
    }
    Map<String, dynamic> body = {'pId': ids};
    http.Response response =
        await bc.post(Uri.http(url, '/roles/$id/permissions/'),
            headers: <String, String>{
              'Content-Type': 'appliction/json',
            },
            body: jsonEncode(body));
    return _processResponse(response);
  }

  Future<String> deletePermissionFromRole(
    List<int> pId,
    int id,
  ) async {
    List<Map<String, dynamic>> ids = [];
    for (int id in pId) {
      ids.add({'id': id.toString()});
    }
    Map<String, dynamic> body = {'pId': ids};

    http.Response response =
        await http.delete(Uri.http(url, '/roles/$id/permissions/'),
            headers: <String, String>{
              'Content-Type': 'application/json',
            },
            body: jsonEncode(body));
    return _processResponse(response);
  }

  Future<String> addOrRemovePermissions(
      String userId, List<Map<String, dynamic>> _permissions) async {
    http.Response response = await bc.post(
      Uri.http(url, '/users/permissions/$userId/'),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body: {
        'permissions': _permissions,
      },
    );

    return _processResponse(response);
  }

  Future<String> getAllPermissions() async {
    http.Response response = await bc.get(
        Uri.http(
          url,
          '/permissions/',
        ),
        headers: <String, String>{
          'Content-Type': 'appliction/json',
        });
    return _processResponse(response);
  }

  Future<String> _processResponse(http.Response response) async {
    switch (response.statusCode) {
      case 200:
        {
          return response.body;
        }
      case 400:
        {
          throw NetworkException(message: '400', logMessage: '400');
        }
      case 401:
        {
          throw NetworkException(message: '401');
        }
      case 403:
        {
          throw NetworkException(message: '403');
        }
      case 404:
        {
          throw NetworkException(message: '404');
        }
      case 408:
        {
          throw NetworkException(message: '408');
        }
      case 409:
        {
          throw DatabaseException(message: '409');
        }
      default:
        {
          throw NetworkException(message: "Unknown Error");
        }
    }
  }
}
