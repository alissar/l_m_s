import 'package:http/http.dart' as http;
import 'package:whatever/service/service.dart';
import 'dart:convert';

import 'package:whatever/utils/exception.dart';

class ResetPasswordRequestService extends Service {
  Future<void> resetPasswordRequest(String email) async {
    print(email);
    http.Response response = await bc.post(
      Uri.http(url, '/reset-password-request/'),
      headers: <String, String>{
        'Content-type': 'application/json',
      },
      body: jsonEncode(<String, String>{
        'email': email,
      }),
    );

    _processResponse(response);
  }

  String _processResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        {
          return response.body;
        }
      case 400:
        {
          throw NetworkException(message: '400');
        }
      case 401:
        {
          throw NetworkException(message: '401');
        }
      case 403:
        {
          throw NetworkException(message: '403');
        }
      case 404:
        {
          throw NetworkException(message: '404');
        }
      case 408:
        {
          throw NetworkException(message: '408');
        }
      case 409:
        {
          throw DatabaseException(message: '409');
        }
      default:
        {
          throw NetworkException(message: "Unknown Error");
        }
    }
  }
}
