import 'package:http/http.dart' as http;
import 'package:whatever/service/service.dart';
import 'dart:convert';

import 'package:whatever/utils/exception.dart';

class RolesService extends Service {
  var dDay = DateTime.utc(1944, 6, 6);

  Future<String> createNewRole(String name, String description, String order,
      List<int> permissions) async {
    List<Map<String, dynamic>> ids = [];
    for (int perId in permissions) {
      ids.add({'id': perId.toString()});
    }
    Map<String, dynamic> body = {
      'name': name,
      'description': description,
      'r_order': order,
      'permissions': ids,
    };

    http.Response response = await bc.post(
      Uri.http(url, '/roles/'),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body: body,
    );
    return _processResponse(response);
  }

  Future<String> assignRoleTo(
      double role, String id, double context, dDay) async {
    Map<String, dynamic> queryParams = {
      'Role': role,
      'id': id,
      'context': context,
      'date': dDay,
    };
    http.Response response = await bc.post(
        Uri.http(url, '/Roles/AssignRole/', queryParams),
        headers: <String, String>{
          'Content-Type': 'appliction/json',
        });
    return _processResponse(response);
  }

  Future<String> getRole(String id, String context) async {
    http.Response response = await bc.get(
        Uri.http(
          url,
          '/$context/users/$id/roles/',
        ),
        headers: <String, String>{
          'Content-Type': 'appliction/json',
        });
    return _processResponse(response);
  }

  Future<String> getAllRoles() async {
    http.Response response = await bc.get(
      Uri.http(url, '/roles/'),
      headers: <String, String>{
        'Content-Type': 'appliction/json',
      },
    );

    return _processResponse(response);
  }

  Future<String> getRolePermissions(int roleId, int page, int size) async {
    http.Response response = await bc.get(
      Uri.http(url, '/roles/$roleId/permissions/$page/$size/'),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
    );

    return _processResponse(response);
  }

  Future<String> _processResponse(http.Response response) async {
    switch (response.statusCode) {
      case 200:
        {
          return response.body;
        }
      case 400:
        {
          throw NetworkException(message: '400', logMessage: '400');
        }
      case 401:
        {
          throw NetworkException(message: '401');
        }
      case 403:
        {
          throw NetworkException(message: '403');
        }
      case 404:
        {
          throw NetworkException(message: '404');
        }
      case 408:
        {
          throw NetworkException(message: '408');
        }
      case 409:
        {
          throw DatabaseException(message: '409');
        }
      default:
        {
          throw NetworkException(message: "Unknown Error");
        }
    }
  }
}
