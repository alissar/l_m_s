import 'package:http/http.dart' as http;
import 'package:whatever/service/service.dart';
import 'package:whatever/utils/exception.dart';
import 'dart:convert';

class UsersService extends Service {
  Future<String> getAllUsers(int page, int size) async {
    Map<String, dynamic> queryParams = {
      'page': [page.toString()],
      'size': [size.toString()],
    };
    http.Response response = await bc.get(
      Uri.http(url, '/users/', queryParams),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
    );
    return _processResponse(response);
  }

  Future<String> getDeletedUsers(int page, int size) async {
    Map<String, dynamic> queryParams = {
      'page': [page.toString()],
      'size': [size.toString()],
    };
    http.Response response = await bc.get(
      Uri.http(url, '/users/deleted/', queryParams),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
    );
    return _processResponse(response);
  }

  Future<String> searchForUser(int page, int size,
      {String userId,
      String email,
      String nickname,
      String country,
      String city,
      String institution,
      String department,
      String firstName,
      String middleName,
      String lastName,
      String lang}) async {
    Map<String, dynamic> queryParams = {
      'page': page,
      'size': size,
      'userID': userId,
      'email': email,
      'nickName': nickname,
      'country': country,
      'city': city,
      'institution': institution,
      'department': department,
      'firstName': firstName,
      'fatherName': middleName,
      'lastName': lastName,
      'lang': lang,
    };
    queryParams.removeWhere((key, value) => value == null);

    http.Response response = await bc.get(
      Uri.https(url, '/users/search', queryParams),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
    );
    return _processResponse(response);
  }

  Future<String> updateUser(String userId,
      {String firstName,
      String middleName,
      String lastName,
      String nickname,
      String email,
      String institution,
      String department,
      String city,
      String country,
      String lang,
      String picture,
      DateTime timeCreated}) async {
    // TODO add Collection of RoleModel to list of parameters
    // Review word document for reference

    Map<String, dynamic> queryParams = {
      'userID': userId,
      'email': email,
      'nickName': nickname,
      'country': country,
      'city': city,
      'institution': institution,
      'department': department,
      'firstName': firstName,
      'fatherName': middleName,
      'lastName': lastName,
      'lang': lang,
      'picture': picture,
      'timeCreated': timeCreated,
    };
    queryParams.removeWhere((key, value) => value == null);

    http.Response response = await http.put(
      Uri.https(url, '/update', queryParams),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
    );
    return _processResponse(response);
  }

  Future<String> deleteUser(String userId) async {
    // TODO check if userId is correct to be string

    http.Response response = await http.delete(
      Uri.http(url, '/delete/' + userId),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
    );
    return _processResponse(response);
  }

  Future<String> getUserProfile(String userUrl) async {
    http.Response response = await bc.get(
      Uri.http(url, '/$userUrl/'),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
    );

    return _processResponse(response);
  }

  Future<String> _processResponse(http.Response response) async {
    print(response.body);
    switch (response.statusCode) {
      case 200:
        {
          return response.body;
        }
      case 400:
        {
          throw NetworkException(message: '400', logMessage: '400');
        }
      case 401:
        {
          throw NetworkException(message: '401');
        }
      case 403:
        {
          throw NetworkException(message: '403');
        }
      case 404:
        {
          throw NetworkException(message: '404');
        }
      case 408:
        {
          throw NetworkException(message: '408');
        }
      case 409:
        {
          throw DatabaseException(message: '409');
        }
      default:
        {
          throw NetworkException(message: "Unknown Error");
        }
    }
  }
}
