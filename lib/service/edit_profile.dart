import 'package:whatever/service/service.dart';
import 'package:whatever/utils/exception.dart';
import 'package:http/http.dart' as http;

class EditProfileService extends Service {
  Future<String> editProfileInfo(
    String userId,
    String firstName,
    String middleName,
    String lastName,
    String nickname,
    String email,
    String lang,
    String theme,
    String address,
    String institution,
    String department,
    String city,
    String country,
    String imagePath,
    String userUrl,
  ) async {
    http.Response response = await http.put(
        Uri.http(
          url,
          '/users/update/',
        ),
        body: {
          'userID': userId == null ? '-1' : userId,
          'firstName': firstName,
          'fatherName': middleName,
          'lastName': lastName,
          'nickName': nickname,
          'email': email,
          'lang': lang == null ? '-1' : lang,
          'theme': theme == null ? '-1' : theme,
          'institution': institution,
          'department': department,
          'city': city,
          'country': country,
          'imagePath': imagePath == null ? '-1' : imagePath,
          'url': userUrl == null ? '-1' : userUrl,
        });

    print('processing response');

    return _processResponse(response);
  }

  String _processResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        {
          return response.body;
        }
      case 400:
        {
          throw NetworkException(message: '400');
        }
      case 401:
        {
          throw NetworkException(message: '401');
        }
      case 403:
        {
          throw NetworkException(message: '403');
        }
      case 404:
        {
          throw NetworkException(message: '404');
        }
      case 408:
        {
          throw NetworkException(message: '408');
        }
      case 409:
        {
          throw DatabaseException(message: '409');
        }
      default:
        {
          throw NetworkException(message: "Unknown Error");
        }
    }
  }
}
