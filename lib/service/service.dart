import 'package:http/browser_client.dart';

abstract class Service {
  final String url = 'localhost:8080';

  BrowserClient bc;

  Service() {
    bc = BrowserClient();
    bc.withCredentials = true;
  }
}
