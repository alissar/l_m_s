import 'package:http/http.dart' as http;
import 'package:whatever/service/service.dart';
import 'package:whatever/utils/exception.dart';

class ProfileDataService extends Service {
  Future<String> getuserProfileInfo() async {
    http.Response response = await bc.get(
      Uri.http(
        url,
        '/profile/',
      ),
    );

    return _processResponse(response);
  }

  String _processResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        {
          return response.body;
        }
      case 400:
        {
          throw NetworkException(message: '400');
        }
      case 401:
        {
          throw NetworkException(message: '401');
        }
      case 403:
        {
          throw NetworkException(message: '403');
        }
      case 404:
        {
          throw NetworkException(message: '404');
        }
      case 408:
        {
          throw NetworkException(message: '408');
        }
      case 409:
        {
          throw DatabaseException(message: '409');
        }
      default:
        {
          throw NetworkException(message: "Unknown Error");
        }
    }
  }
}
