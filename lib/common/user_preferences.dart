import 'package:shared_preferences/shared_preferences.dart';
import 'package:whatever/model/user_model.dart';

class UserPreferences {
  Future<void> saveUserToken(String token) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("token", token);
  }

  Future<void> saveUser(UserModel user) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setInt("userId", user.id);
    await prefs.setString("firstName", user.firstName);
    await prefs.setString("middleName", user.middleName);
    await prefs.setString("lastName", user.lastName);
    await prefs.setString("nickname", user.nickname);
    await prefs.setString("email", user.email);
    await prefs.setString("phone", user.phone);
    await prefs.setString("address", user.address);
    await prefs.setString("token", user.token);
  }

  Future<UserModel> getUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    int userId = prefs.getInt("userId");
    String firstName = prefs.getString("firstName");
    String middleName = prefs.getString("middleName");
    String lastName = prefs.getString("lastName");
    String nickname = prefs.getString("nickname");
    String email = prefs.getString("email");
    String phone = prefs.getString("phone");
    String address = prefs.getString("address");
    String token = prefs.getString("token");

    return UserModel(
      id: userId,
      firstName: firstName,
      middleName: middleName,
      lastName: lastName,
      nickname: nickname,
      email: email,
      phone: phone,
      address: address,
      token: token,
    );
  }

  void removeUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.remove('userId');
    prefs.remove("firstName");
    prefs.remove("middleName");
    prefs.remove("lastName");
    prefs.remove("nickname");
    prefs.remove("email");
    prefs.remove("phone");
    prefs.remove("address");
    prefs.remove("token");
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return token;
  }

  void unstoreCookie() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.remove('cookie');
  }

  void storeCookie(String cookie) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString('cookie', cookie);
  }
}
