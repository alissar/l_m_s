import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Settings {}

class DefultColors {
  static const Color PRIMARY_LIGHT_COLOR = Color.fromRGBO(79, 179, 191, 1);
  static const Color PRIMARY_DARK_COLOR = Color.fromRGBO(0, 86, 98, 1);
  static const Color PRIMARY_RESET_COLOR = Color.fromRGBO(0, 131, 143, 1);
  static const Color SECONDARY_LIGHT_COLOR = Color.fromRGBO(93, 222, 244, 1);
  static const Color SECONDARY_DARK_COLOR = Color.fromRGBO(0, 124, 145, 1);
  static const Color SECONDARY_RESET_COLOR = Color.fromRGBO(0, 172, 193, 1);
  static const Color LIGHT_COLOR = Color.fromRGBO(254, 254, 254, 1);
  static const Color LOW_LIGHT_COLOR = Color.fromRGBO(244, 245, 239, 1);
  static const Color LOW_YELLOW_COLOR = Color.fromRGBO(253, 185, 84, 1);
}

class DefultFontWeight {
  static const FontWeight BOLD = FontWeight.w900;
  static const FontWeight SEMI_BOLD = FontWeight.w500;
  static const FontWeight MEDIUM = FontWeight.w400;
  static const FontWeight REGULAR = FontWeight.w300;
  static const FontWeight LIGHT = FontWeight.w200;
  static const FontWeight THIN = FontWeight.w100;
}

class DefultFontSize {
  static const double Very_LARGE_FONT_SIZE = 42.0;
  static const double LARGE_FONT_SIZE = 32.0;
  static const double MEDIUM_FONT_SIZE = 24.0;
  static const double SMALL_FONT_SIZE = 18.0;
  static const double VERY_SMALL_FONT_SIZE = 14.0;
}

class DefultBorderRadius {
  static const double HARD_BORDER_RADIUS = 22;
  static const double MEDIUM_BORDER_RADIUS = 20;
  static const double LIGHT_BORDER_RADIUS = 8;
}

class DefultElevationValue {
  static const double MIN_VALUE = 8.0;
  static const double MEDIUM_VALUE = 12.0;
  static const double Larg_VALUE = 20.0;
}

class TextThemes {
  static const TextStyle titlePrimaryText = TextStyle(
    color: DefultColors.LOW_YELLOW_COLOR,
    fontSize: 26.0,
    fontWeight: FontWeight.w600,
    letterSpacing: 1.25,
  );
  static const TextStyle titleLightText = TextStyle(
    color: DefultColors.LIGHT_COLOR,
    fontSize: 26.0,
    fontWeight: FontWeight.w600,
    letterSpacing: 1.25,
  );
  static const TextStyle navBarResetTab = TextStyle(
    color: DefultColors.SECONDARY_RESET_COLOR,
    fontSize: 16.0,
    letterSpacing: 2.75,
  );

  static const TextStyle helpGreyText = TextStyle(
    color: DefultColors.LOW_YELLOW_COLOR,
    fontSize: 16.0,
    letterSpacing: 2.75,
  );
  static const TextStyle helpDarkText = TextStyle(
    color: DefultColors.PRIMARY_DARK_COLOR,
    fontSize: 16.0,
    letterSpacing: 2.75,
  );

  static const TextStyle loginWhiteText = TextStyle(
    color: Colors.white,
    fontSize: 16.0,
    letterSpacing: 2.75,
  );

  static const TextStyle createAccountPrimaryText = TextStyle(
    color: DefultColors.PRIMARY_LIGHT_COLOR,
    fontSize: 16.0,
    letterSpacing: 2.75,
  );

  static const TextStyle yellowTitle = TextStyle(
      color: DefultColors.LOW_YELLOW_COLOR,
      fontSize: 68.0,
      fontWeight: FontWeight.w600);

  static const TextStyle homeDarkTitle = TextStyle(
    color: DefultColors.PRIMARY_DARK_COLOR,
    fontSize: 68.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle homeSubTitle = TextStyle(
    color: Colors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    letterSpacing: 1.0,
  );

  static const TextStyle buttonText = TextStyle(
    color: Colors.white,
    fontSize: 16.0,
  );

  static const TextStyle bigTextTitle = TextStyle(
    color: DefultColors.PRIMARY_DARK_COLOR,
    fontSize: 62.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle descriptionText = TextStyle(
    color: Colors.white38,
    fontSize: 18.0,
    wordSpacing: 1.75,
    letterSpacing: 1.75,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle whititleText = TextStyle(
    color: Colors.white,
    fontSize: 60.0,
    fontWeight: FontWeight.w600,
    letterSpacing: 0.75,
  );

  static const TextStyle createText = TextStyle(
    color: Colors.white,
    fontSize: 28.0,
  );

  static const TextStyle whitexlText = TextStyle(
    color: Colors.grey,
    fontSize: 16.0,
  );

  static const TextStyle howitworkDec = TextStyle(
    color: Colors.grey,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle purpuleText = TextStyle(
    color: DefultColors.PRIMARY_DARK_COLOR,
    fontSize: 58.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle cardText = TextStyle(
    color: DefultColors.PRIMARY_DARK_COLOR,
    fontSize: 26.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle footerText =
      TextStyle(color: Colors.grey, fontSize: 14.0);

  static const TextStyle pinkText = TextStyle(
      color: Colors.pinkAccent, fontSize: 20.0, fontWeight: FontWeight.w600);

  static const TextStyle smallWhiteText = TextStyle(
      color: DefultColors.PRIMARY_DARK_COLOR,
      fontSize: 18.0,
      fontWeight: FontWeight.w600);
}
