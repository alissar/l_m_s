import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';

class CostumTextFeildWidget extends StatelessWidget {
  final String _hint;
  // final Widget _suffixIcon;
  final bool _obsecureText;
  final TextEditingController _controller;

  CostumTextFeildWidget(
      {String hint,
      Widget suffixIcon,
      bool obsecureText,
      TextEditingController controller})
      : _hint = hint,
        // _suffixIcon = suffixIcon,
        _obsecureText = obsecureText,
        _controller = controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: _controller,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration: InputDecoration(
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: DefultColors.PRIMARY_DARK_COLOR),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: DefultColors.LOW_LIGHT_COLOR),
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.redAccent),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: DefultColors.LOW_LIGHT_COLOR),
          ),
          hintText: _hint,
          hintStyle: TextStyle(
              fontSize: DefultFontSize.VERY_SMALL_FONT_SIZE,
              fontWeight: DefultFontWeight.THIN,
              color: DefultColors.SECONDARY_LIGHT_COLOR),
          // suffixIcon: _suffixIcon,
        ),
        obscureText: _obsecureText,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "please enter some text ";
          }
          return null;
        },
      ),
    );
  }
}
