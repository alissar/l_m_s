import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';

class TitledTextFeildWithBordersClass extends StatelessWidget {
  final String _hint;
  final String _title;
  final Widget _suffixIcon;
  final bool _editable;
  final bool _obsecureText;
  final TextEditingController _controller;

  TitledTextFeildWithBordersClass(
      {String hint,
      bool editable = false,
      Widget suffixIcon,
      bool obsecureText,
      String title,
      TextEditingController controller})
      : _hint = hint,
        _editable = editable,
        _title = title,
        _suffixIcon = suffixIcon,
        _obsecureText = obsecureText,
        _controller = controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.07,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
            child: Center(
              child: Text(
                _title,
                style: TextStyle(
                    color: DefultColors.PRIMARY_DARK_COLOR,
                    fontWeight: DefultFontWeight.BOLD,
                    fontSize: DefultFontSize.SMALL_FONT_SIZE),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.15,
            height: MediaQuery.of(context).size.height * 0.05,
            padding: EdgeInsets.symmetric(vertical: 0.0),
            child: TextFormField(
              enabled: _editable,
              controller: _controller,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: DefultColors.SECONDARY_DARK_COLOR)),
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: DefultColors.SECONDARY_RESET_COLOR),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.redAccent),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: DefultColors.SECONDARY_RESET_COLOR),
                ),
                hintText: _hint,
                hintStyle: TextStyle(
                    fontSize: DefultFontSize.VERY_SMALL_FONT_SIZE,
                    fontWeight: DefultFontWeight.THIN,
                    color: DefultColors.SECONDARY_LIGHT_COLOR),
                suffixIcon: _suffixIcon,
              ),
              obscureText: _obsecureText,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "please enter some text ";
                }
                return null;
              },
            ),
          ),
        ],
      ),
    );
  }
}
