import 'package:flutter/cupertino.dart';

class ResponsiveColumn extends StatelessWidget {
  final List<Widget> children;

  ResponsiveColumn({@required this.children});
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) =>
            SingleChildScrollView(
                child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: viewportConstraints.maxHeight,
                    ),
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: children
                            .map((e) => IntrinsicHeight(
                                // constraints: viewportConstraints.loosen(),
                                child: e))
                            .toList()))));
  }
}
