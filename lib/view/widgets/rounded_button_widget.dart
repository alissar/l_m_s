import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';


class RoundedButton extends StatelessWidget {
  RoundedButton({this.textTitle, this.color});

  final Color color;
  final String textTitle;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: MaterialButton(
        height: size.height * 0.09,
        minWidth: size.width * 0.15,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26.0),
        ),
        onPressed: () {},
        color: color,
        child: Text(textTitle.toUpperCase(),
          style: TextThemes.buttonText,
        ),
      ),
    );
  }
}
