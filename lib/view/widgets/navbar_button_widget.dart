import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/sign_in_screen.dart';

class NavbarButtonWidget extends StatelessWidget {
  @required
  final String _text;
  NavbarButtonWidget({String text}) : _text = text;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(color: DefultColors.PRIMARY_DARK_COLOR),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
          child: Text(
            _text,
            style: (TextStyle(
                color: DefultColors.PRIMARY_DARK_COLOR,
                fontWeight: DefultFontWeight.BOLD)),
          ),
        ),
      ),
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => SignInScreen()));
      },
    );
  }
}
