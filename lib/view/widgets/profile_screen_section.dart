import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/widgets/titled_text_feild_with_border.dart';

class ProfileScreenSection extends StatefulWidget {
  final String name;
  final List<TitledTextFeildWithBordersClass> fields;

  const ProfileScreenSection(
      {Key key, @required this.name, @required this.fields})
      : super(key: key);

  @override
  _ProfileScreenSectionState createState() => _ProfileScreenSectionState();
}

class _ProfileScreenSectionState extends State<ProfileScreenSection> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 75.0, vertical: 40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.name,
            style: TextStyle(
              color: DefultColors.PRIMARY_DARK_COLOR,
              fontWeight: DefultFontWeight.BOLD,
              fontSize: DefultFontSize.LARGE_FONT_SIZE,
            ),
          ),
          Column(
            children: widget.fields,
          )
        ],
      ),
    );
  }
}
