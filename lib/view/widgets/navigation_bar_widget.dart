import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/sign_in_screen.dart';

class NavigationBarWidget extends StatefulWidget {
  @override
  _NavigationBarWidgetState createState() => _NavigationBarWidgetState();
}

class _NavigationBarWidgetState extends State<NavigationBarWidget> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        if (constraints.maxWidth >= 1200) {
          //Desktop
          return DesktopNavBar();
        } else if (constraints.maxWidth >= 800 &&
            constraints.maxWidth <= 1200) {
          //Tablet
          return DesktopNavBar();
        } else {
          //Mobile
          return MobileNavBar();
        }
      },
    );
  }
}

class DesktopNavBar extends StatefulWidget {
  @override
  _DesktopNavBarState createState() => _DesktopNavBarState();
}

class _DesktopNavBarState extends State<DesktopNavBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.15,
      color: DefultColors.PRIMARY_DARK_COLOR,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Wrap(
                    alignment: WrapAlignment.center,
                    children: [
                      Text(
                        "L".toUpperCase(),
                        style: TextThemes.titlePrimaryText,
                      ),
                      Text("MS".toUpperCase(), style: TextThemes.titleLightText)
                    ],
                  ),
                  GestureDetector(
                    child: Text("Home", style: TextThemes.loginWhiteText),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SignInScreen()));
                    },
                  ),
                  GestureDetector(
                    child: Text("About", style: TextThemes.loginWhiteText),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SignInScreen()));
                    },
                  ),
                  GestureDetector(
                    child: Text("Pages", style: TextThemes.loginWhiteText),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SignInScreen()));
                    },
                  ),
                  GestureDetector(
                    child: Text("News", style: TextThemes.loginWhiteText),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SignInScreen()));
                    },
                  ),
                  GestureDetector(
                    child: Text("Contact", style: TextThemes.loginWhiteText),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SignInScreen()));
                    },
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(),
            ),
            Flexible(
              flex: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text("Help", style: TextThemes.helpGreyText),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              width: 0.75, height: 16, color: Colors.white),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child:
                              Text("Login", style: TextThemes.loginWhiteText),
                        ),
                      ],
                    ),
                  ),
                  Text("Create an account",
                      style: TextThemes.createAccountPrimaryText),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//Mobile NavBar

class MobileNavBar extends StatefulWidget {
  @override
  _MobileNavBarState createState() => _MobileNavBarState();
}

class _MobileNavBarState extends State<MobileNavBar> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
