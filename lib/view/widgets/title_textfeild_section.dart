import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';

class TitleTextFeildSectionClass extends StatelessWidget {
  final String _hint;
  final String _title;
  final Widget _suffixIcon;
  final bool _obsecureText;
  final TextEditingController _controller;

  TitleTextFeildSectionClass(
      {String hint,
      Widget suffixIcon,
      bool obsecureText,
      String title,
      TextEditingController controller})
      : _hint = hint,
        _title = title,
        _suffixIcon = suffixIcon,
        _obsecureText = obsecureText,
        _controller = controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.07,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
            child: Center(
              child: Text(
                _title,
                style: TextStyle(
                    color: DefultColors.PRIMARY_DARK_COLOR,
                    fontWeight: DefultFontWeight.MEDIUM,
                    fontSize: DefultFontSize.VERY_SMALL_FONT_SIZE),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.15,
            height: MediaQuery.of(context).size.height * 0.05,
            padding: EdgeInsets.symmetric(vertical: 0.0),
            child: TextFormField(
              controller: _controller,
              // autovalidateMode: AutovalidateMode.onUserInteraction,
              decoration: InputDecoration(
                border: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: DefultColors.SECONDARY_RESET_COLOR),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: DefultColors.SECONDARY_RESET_COLOR),
                ),
                errorBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.redAccent),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: DefultColors.SECONDARY_RESET_COLOR),
                ),
                hintText: _hint,
                hintStyle: TextStyle(
                    fontSize: DefultFontSize.VERY_SMALL_FONT_SIZE,
                    fontWeight: DefultFontWeight.THIN,
                    color: DefultColors.SECONDARY_LIGHT_COLOR),
                suffixIcon: _suffixIcon,
              ),
              obscureText: _obsecureText,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "please enter some text ";
                }
                return null;
              },
            ),
          ),
        ],
      ),
    );
  }
}
