import 'dart:typed_data';

import 'package:country_picker/country_picker.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/permissions_event.dart';
import 'package:whatever/bloc/permissions_state.dart';
import 'package:whatever/bloc/role_bloc.dart';
import 'package:whatever/bloc/role_events.dart';
import 'package:whatever/bloc/sign_up_bloc.dart';
import 'package:whatever/bloc/sign_up_event.dart';
import 'package:whatever/bloc/sign_up_state.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/view/screens/profile_screen.dart';
import 'package:whatever/view/widgets/title_textfeild_section.dart';

class CreateRoleScreen extends StatefulWidget {
  CreateRoleScreen({Key key}) : super(key: key);

  @override
  _CreateRoleScreenState createState() => _CreateRoleScreenState();
}

class _CreateRoleScreenState extends State<CreateRoleScreen> {
  String _languageChosenValue;
  bool _obsecureText = true;
  List<PermissionsModel> pickedPermissions;

  void _togglevisibility() {
    setState(() {
      _obsecureText = !_obsecureText;
    });
  }

  TextEditingController roleNameTextEditingController = TextEditingController();
  TextEditingController roleDescriptionTextEditingController =
      TextEditingController();
  TextEditingController orderTextEditingController = TextEditingController();

  List<DropdownMenuItem> _layoutDropdownItems(
      List<PermissionsModel> allPermissions) {
    List<DropdownMenuItem> dropdownMenuItems = [];

    for (PermissionsModel permission in allPermissions) {
      List<PermissionsModel> foundPermissions = pickedPermissions
          .where((element) => element.id == permission.id)
          .toList();
      if (foundPermissions.length == 0) {
        dropdownMenuItems.add(
          DropdownMenuItem(
            onTap: () {
              setState(() {
                pickedPermissions.add(permission);
              });
            },
            child: Text(permission.perName),
            value: permission.id,
          ),
        );
      }
    }

    return dropdownMenuItems;
  }

  List<Widget> _layoutPickedPermissions() {
    List<Widget> children = [];
    for (PermissionsModel permission in pickedPermissions) {
      children.add(Text(permission.perName));
    }

    return children;
  }

  @override
  void initState() {
    super.initState();

    pickedPermissions = [];
    BlocProvider.of<PermissionsBloc>(context).add(AllPermissionsRequested());
  }

  @override
  Widget build(BuildContext context) {
    final double widthSize = MediaQuery.of(context).size.width;
    final double heightSize = MediaQuery.of(context).size.height;

    return Container(
      color: DefultColors.LOW_LIGHT_COLOR,
      child: Center(
        child: Container(
          height: heightSize,
          width: widthSize * 0.6,
          child: Card(
            child: Material(
              child: Stack(children: [
                Container(
                  width: widthSize,
                  height: heightSize,
                  child: Image(
                    image: AssetImage('assets/images/sign_up.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(flex: 6, child: Container()),
                    Expanded(
                      flex: 7,
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          children: [
                            Expanded(
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    SizedBox(
                                      child: Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.03,
                                      ),
                                    ),
                                    Container(
                                      child: Form(
                                        child: Column(
                                          children: [
                                            TitleTextFeildSectionClass(
                                              controller:
                                                  roleNameTextEditingController,
                                              obsecureText: false,
                                              title: 'Role Name',
                                            ),
                                            TitleTextFeildSectionClass(
                                              controller:
                                                  roleDescriptionTextEditingController,
                                              obsecureText: false,
                                              title: 'Role Description',
                                            ),
                                            TitleTextFeildSectionClass(
                                              controller:
                                                  orderTextEditingController,
                                              obsecureText: false,
                                              title: 'Order',
                                            ),
                                            Column(
                                              children:
                                                  _layoutPickedPermissions(),
                                            ),
                                            BlocBuilder<PermissionsBloc,
                                                PermissionsState>(
                                              builder: (context, state) {
                                                if (state
                                                    is PermissionsLoadInProgress) {
                                                  return CircularProgressIndicator();
                                                }
                                                if (state
                                                    is PermissionsLoadSuccess) {
                                                  return Container(
                                                    height: 50,
                                                    width: 150,
                                                    child: DropdownButton(
                                                      onChanged: (value) {},
                                                      items:
                                                          _layoutDropdownItems(
                                                              state
                                                                  .permissions),
                                                    ),
                                                  );
                                                }
                                                return Container();
                                              },
                                            ),
                                            ElevatedButton(
                                              child: Text('Submit'),
                                              onPressed: () async {
                                                BlocProvider.of<RoleBloc>(
                                                        context)
                                                    .add(CreateNewRole(
                                                  name:
                                                      roleNameTextEditingController
                                                          .text,
                                                  description:
                                                      roleDescriptionTextEditingController
                                                          .text,
                                                  order:
                                                      orderTextEditingController
                                                          .text,
                                                  permissions: [],
                                                ));
                                              },
                                              style: ElevatedButton.styleFrom(
                                                primary: DefultColors
                                                    .PRIMARY_RESET_COLOR,
                                              ),
                                            ),
                                            SizedBox(
                                              child: Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.03,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Positioned(
                  top: 28,
                  left: 125,
                  height: 150,
                  width: 150,
                  child: Container(
                    child: Text(
                      'Sign up',
                      style: TextStyle(
                          color: DefultColors.LOW_LIGHT_COLOR,
                          fontWeight: DefultFontWeight.BOLD,
                          fontSize: DefultFontSize.Very_LARGE_FONT_SIZE,
                          fontFamily: 'Roboto'),
                    ),
                  ),
                ),
                Positioned(
                  top: 82,
                  left: 100,
                  height: 150,
                  width: 200,
                  child: Text(
                    'Build skills with courses,\ncertificates, and degrees\n               online',
                    style: TextStyle(
                        color: DefultColors.LOW_LIGHT_COLOR,
                        fontSize: DefultFontSize.SMALL_FONT_SIZE,
                        fontWeight: DefultFontWeight.THIN,
                        fontFamily: 'Roboto'),
                  ),
                )
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
