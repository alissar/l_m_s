import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:whatever/bloc/profile_bloc.dart';
import 'package:whatever/bloc/profile_events.dart';
import 'package:whatever/bloc/profile_states.dart';
import 'package:whatever/bloc/reset_password_bloc.dart';
import 'package:whatever/bloc/reset_password_event.dart';
import 'package:whatever/bloc/reset_password_state.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/common/user_preferences.dart';
import 'package:whatever/view/widgets/layout/responsive_flex.dart';
import 'package:whatever/view/widgets/profile_screen_section.dart';
import 'package:whatever/view/widgets/titled_text_feild_with_border.dart';

class ResetPasswordScreen extends StatefulWidget {
  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  TextEditingController _passwordEditingController = TextEditingController();
  TextEditingController _passwordConfirmationEditingController =
      TextEditingController();

  Future<void> _initProfileEvent() async {
    int id = (await UserPreferences().getUser()).id;
    BlocProvider.of<ProfileBloc>(context).add(ProfileRequested());
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProfileBloc>(context).add(ProfileRequested());
  }

  @override
  Widget build(BuildContext context) {
    _initProfileEvent();

    final double widthSize = MediaQuery.of(context).size.width;
    final double heightSize = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: heightSize * 0.5,
              child: Stack(children: [
                ResponsiveFlex(
                  direction: Axis.vertical,
                  children: [
                    Container(
                      width: widthSize,
                      height: heightSize * 0.2,
                      child: Image(
                        fit: BoxFit.cover,
                        image: AssetImage('assets/images/profile.png'),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      alignment: Alignment.topRight,
                      padding: EdgeInsets.all(10),
                      child: ElevatedButton.icon(
                        label: Text('Edit Profile'),
                        icon: Icon(Icons.edit),
                        style: ElevatedButton.styleFrom(
                            elevation: 20,
                            shadowColor: Colors.grey,
                            primary: DefultColors.PRIMARY_DARK_COLOR),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
                Positioned(
                  top: 180,
                  left: 100,
                  width: 600,
                  child: Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: DefultColors.PRIMARY_DARK_COLOR,
                        radius: 70.0,
                        child: CircleAvatar(
                          radius: 60.0,
                          backgroundColor: Colors.white,
                          foregroundImage:
                              AssetImage('assets/images/signIn.jpg'),
                        ),
                      ),
                      BlocBuilder<ProfileBloc, ProfileState>(
                        builder: (context, state) {
                          if (state is ProfileLoadInProgress) {
                            return CircularProgressIndicator();
                          }
                          if (state is ProfileLoadSuccess) {
                            return Container(
                              padding: EdgeInsets.only(left: 30),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    state.user.firstName +
                                        ' ' +
                                        state.user.lastName,
                                    style: TextStyle(
                                        fontSize:
                                            DefultFontSize.MEDIUM_FONT_SIZE,
                                        fontWeight: DefultFontWeight.BOLD,
                                        color: Colors.white,
                                        fontFamily: 'Roboto'),
                                  ),
                                  Text(
                                    state.user.email,
                                    style: TextStyle(
                                        fontSize:
                                            DefultFontSize.VERY_SMALL_FONT_SIZE,
                                        fontWeight: DefultFontWeight.BOLD,
                                        color: Colors.white,
                                        fontFamily: 'Roboto'),
                                  ),
                                ],
                              ),
                            );
                          }
                          if (state is ProfileLoadFailure) {
                            return Center(
                              child: Text(state.error),
                            );
                          }
                          return Container();
                        },
                      ),
                    ],
                  ),
                ),
              ]),
            ),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: BlocBuilder<ResetPasswordBloc, ResetPasswordState>(
                      builder: (context, state) {
                        if (state is ResetPasswordInitialState) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              ProfileScreenSection(
                                name: 'Password Reset :',
                                fields: [
                                  TitledTextFeildWithBordersClass(
                                    editable: true,
                                    controller: _passwordEditingController,
                                    obsecureText: false,
                                    title: 'New Password',
                                  ),
                                  TitledTextFeildWithBordersClass(
                                    editable: true,
                                    controller:
                                        _passwordConfirmationEditingController,
                                    obsecureText: false,
                                    title: 'Confirm Password',
                                  ),
                                ],
                              ),
                              Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(10),
                                child: ElevatedButton(
                                  child: Text('Reset password'),
                                  style: ElevatedButton.styleFrom(
                                      elevation: 20,
                                      shadowColor: Colors.grey,
                                      primary: DefultColors.PRIMARY_DARK_COLOR),
                                  onPressed: () {
                                    if (_passwordEditingController.text !=
                                        _passwordConfirmationEditingController
                                            .text) {
                                      Fluttertoast.showToast(
                                          msg: 'Passwords don\'t match');
                                      return;
                                    }
                                    BlocProvider.of<ResetPasswordBloc>(context)
                                        .add(ResetPassword(
                                            newPassword:
                                                _passwordEditingController
                                                    .text));
                                  },
                                ),
                              ),
                            ],
                          );
                        }
                        if (state is ResetPasswordInProgress) {
                          return CircularProgressIndicator();
                        }
                        if (state is ResetPasswordLoadSuccess) {
                          _onWidgetDidBuild(() {
                            BlocProvider.of<ResetPasswordBloc>(context)
                                .add(Reset());
                            Fluttertoast.showToast(msg: 'Success');
                          });
                          return Container();
                        }
                        if (state is ResetPasswordLoadOnFailure) {
                          return Center(
                            child: Text(state.error),
                          );
                        }
                        return Container();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
