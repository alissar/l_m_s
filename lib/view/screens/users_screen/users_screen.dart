import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/user_bloc.dart';
import 'package:whatever/bloc/user_events.dart';
import 'package:whatever/bloc/user_states.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/dashboard/responsive.dart';
import 'components/recent_files.dart';
import 'components/storage_details.dart';

class UsersScreen extends StatefulWidget {
  @override
  _UsersScreenState createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void initState() {
    super.initState();

    BlocProvider.of<UserBloc>(context).add(GetAllUsers(page: 1));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            SizedBox(height: defaultPadding),
            BlocBuilder<UserBloc, UserState>(builder: (context, state) {
              if (state is UserLoadInProgress) {
                return CircularProgressIndicator();
              }
              if (state is UserLoadSuccessList) {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          SizedBox(height: defaultPadding),
                          RecentFiles(
                            subTitle0: 'Name',
                            subTitle1: 'Email',
                            subTitle2: 'Permissions',
                            subTitle3: 'Delete User',
                            subTitle4: 'Roles',
                            dataString: 'Permissions',
                            title: 'Users',
                            length: state.users.length,
                            users: state.users,
                            deletedUSers: false,
                          ),
                          SizedBox(height: 20),
                          RecentFiles(
                            subTitle0: 'Name',
                            subTitle1: 'Email',
                            subTitle2: 'Phone',
                            subTitle3: 'Deleted',
                            subTitle4: 'Roles',
                            title: 'Deleted Users',
                            length: state.deletedUsers.length,
                            users: state.deletedUsers,
                          ),
                          if (Responsive.isMobile(context))
                            SizedBox(height: defaultPadding),
                          if (Responsive.isMobile(context)) StarageDetails(),
                        ],
                      ),
                    ),
                    if (!Responsive.isMobile(context))
                      SizedBox(width: defaultPadding),
                  ],
                );
              }
              if (state is UsersLoadFailure) {
                return Container(
                  child: Text('Something went wrong'),
                  height: 30,
                  width: 40,
                  color: Colors.red,
                );
              }

              return Container(
                height: 20,
              );
            }),
          ],
        ),
      ),
    );
  }
}
