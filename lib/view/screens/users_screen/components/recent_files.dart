import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/role_bloc.dart';
import 'package:whatever/bloc/role_events.dart';
import 'package:whatever/bloc/user_bloc.dart';
import 'package:whatever/bloc/user_events.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/other_profile_screen.dart';
import 'package:whatever/view/screens/roles_screen/roles_screen.dart';
import 'package:whatever/view/screens/user_Permissions_screen/UserPermissionsScreen.dart';

class RecentFiles extends StatefulWidget {
  final List<UserModel> users;
  final int length;
  final String title;
  final String dataString;
  final String subTitle0;
  final String subTitle1;
  final String subTitle2;
  final String subTitle3;
  final String subTitle4;
  final bool deletedUSers;
  const RecentFiles({
    Key key,
    this.users,
    this.length,
    this.title,
    this.dataString,
    this.subTitle0,
    this.subTitle1,
    this.subTitle2,
    this.subTitle3,
    this.subTitle4,
    this.deletedUSers,
  }) : super(key: key);

  @override
  _RecentFilesState createState() => _RecentFilesState();
}

class _RecentFilesState extends State<RecentFiles> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        padding: EdgeInsets.all(defaultPadding),
        decoration: BoxDecoration(
          color: DefultColors.PRIMARY_DARK_COLOR,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text(widget.title,
                  style: TextStyle(color: DefultColors.LIGHT_COLOR)),
            ),
            SizedBox(height: 28),
            SizedBox(
              width: double.infinity,
              child: DataTable2(
                columnSpacing: defaultPadding,
                minWidth: 600,
                columns: [
                  DataColumn(
                    label: Text(
                      widget.subTitle0,
                      style: TextStyle(color: DefultColors.LIGHT_COLOR),
                    ),
                  ),
                  DataColumn(
                    label: Text(widget.subTitle1,
                        style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                  ),
                  DataColumn(
                    label: Text(widget.subTitle2,
                        style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                  ),
                  DataColumn(
                    label: Text(widget.subTitle4,
                        style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                  ),
                  DataColumn(
                    label: Text(widget.subTitle3,
                        style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                  ),
                ],
                rows: List.generate(
                  widget.length,
                  (index) => userDataRow(widget.users[index], context,
                      widget.dataString, widget.deletedUSers),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

DataRow userDataRow(
    UserModel userModel, context, String dataString, bool deletedUsers) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => OtherProfileScreen(
                        url: userModel.url,
                      ),
                    ),
                  );
                },
                child: Text(
                  userModel.firstName + ' ' + userModel.lastName,
                  style: TextStyle(color: DefultColors.LIGHT_COLOR),
                ),
              ),
            ),
          ],
        ),
      ),
      DataCell(
        Text(
          userModel.email,
          style: TextStyle(color: DefultColors.LIGHT_COLOR),
        ),
      ),
      DataCell(
        InkWell(
          onTap: () {
            if (!deletedUsers) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => UserPermissionsScreen(
                    id: userModel.id,
                  ),
                ),
              );
            }
          },
          child: Text(
            dataString == null ? userModel.phone : dataString,
            style: TextStyle(color: DefultColors.LIGHT_COLOR),
          ),
        ),
      ),
      DataCell(
        InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => RolesScreen(),
            ));
          },
          child: Text(
            'Roles',
            style: TextStyle(color: DefultColors.LIGHT_COLOR),
          ),
        ),
      ),
      DataCell(InkWell(
        onTap: () {
          BlocProvider.of<UserBloc>(context)
              .add(DeleteUser(userId: userModel.id.toString()));
        },
        child: Text(
          'Delete User',
          style: TextStyle(color: DefultColors.LIGHT_COLOR),
        ),
      ))
    ],
  );
}
