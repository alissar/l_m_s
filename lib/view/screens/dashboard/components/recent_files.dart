import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/user_bloc.dart';
import 'package:whatever/bloc/user_events.dart';
import 'package:whatever/bloc/user_states.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';

class RecentFiles extends StatefulWidget {
  const RecentFiles({
    Key key,
  }) : super(key: key);

  @override
  _RecentFilesState createState() => _RecentFilesState();
}

class _RecentFilesState extends State<RecentFiles> {
  @override
  void initState() {
    BlocProvider.of<UserBloc>(context).add(GetAllUsers());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 16,
        ),
        Container(
          padding: EdgeInsets.all(defaultPadding),
          decoration: BoxDecoration(
            color: DefultColors.PRIMARY_DARK_COLOR,
            borderRadius: const BorderRadius.all(Radius.circular(10)),
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.2,
            child: Container(
              alignment: Alignment.center,
              child:
                  BlocBuilder<UserBloc, UserState>(builder: (context, state) {
                if (state is UserLoadInProgress) {
                  return CircularProgressIndicator();
                }
                if (state is UserLoadSuccessList) {
                  return DataTable2(
                    minWidth: 50,
                    columns: [
                      DataColumn(
                        label: Text(
                          "Users",
                          style: TextStyle(
                            color: DefultColors.LIGHT_COLOR,
                            fontSize: DefultFontSize.SMALL_FONT_SIZE,
                          ),
                        ),
                      ),
                    ],
                    rows: List.generate(
                      state.users.length,
                      (index) => recentFileDataRow(state.users[index]),
                    ),
                  );
                }
                if (state is UsersLoadFailure) {
                  return Container(
                    child: Text(state.error),
                  );
                }
                return Container();
              }),
            ),
          ),
        ),
        SizedBox(
          width: 16,
        ),
        // Container(
        //   alignment: Alignment.center,
        //   child: BlocBuilder<UserBloc, UserState>(builder: (context, state) {
        //     if (state is UserLoadInProgress) {
        //       return CircularProgressIndicator();
        //     }
        //     if (state is UserLoadSuccessList) {
        //       return DataTable2(
        //         minWidth: 50,
        //         columns: [
        //           DataColumn(
        //             label: Text(
        //               "Users",
        //               style: TextStyle(
        //                 color: DefultColors.LIGHT_COLOR,
        //                 fontSize: DefultFontSize.SMALL_FONT_SIZE,
        //               ),
        //             ),
        //           ),
        //         ],
        //         rows: List.generate(
        //           state.users.length,
        //           (index) => recentFileDataRow(state.users[index]),
        //         ),
        //       );
        //     }
        //     if (state is UsersLoadFailure) {
        //       return Container(
        //         child: Text(state.error),
        //       );
        //     }
        //     return Container();
        //   }),
        // ),

        // SizedBox(
        //   width: 16,
        // ),
        // Container(
        //   padding: EdgeInsets.all(defaultPadding),
        //   decoration: BoxDecoration(
        //     color: DefultColors.PRIMARY_DARK_COLOR,
        //     borderRadius: const BorderRadius.all(Radius.circular(10)),
        //   ),
        //   child: Row(
        //     crossAxisAlignment: CrossAxisAlignment.center,
        //     children: [
        //       SizedBox(
        //         width: MediaQuery.of(context).size.width * 0.2,
        //         child: Container(
        //           alignment: Alignment.center,
        //           child: DataTable2(
        //             minWidth: 50,
        //             columns: [
        //               DataColumn(
        //                 label: Text(
        //                   "Users",
        //                   style: TextStyle(
        //                     color: DefultColors.LIGHT_COLOR,
        //                     fontSize: DefultFontSize.SMALL_FONT_SIZE,
        //                   ),
        //                 ),
        //               ),
        //             ],
        //             rows: List.generate(
        //               demoRecentFiles.length,
        //               (index) => recentFileDataRow(demoRecentFiles[index]),
        //             ),
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
      ],
    );
  }
}

DataRow recentFileDataRow(UserModel fileInfo) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0),
              child: Text(
                fileInfo.firstName + fileInfo.lastName,
                style: TextStyle(color: DefultColors.LIGHT_COLOR),
              ),
            ),
          ],
        ),
      ),
      // DataCell(
      //   Row(
      //     children: [
      //       Padding(
      //         padding: const EdgeInsets.symmetric(horizontal: 0),
      //         child: Text(
      //           fileInfo.phone,
      //           style: TextStyle(color: DefultColors.LIGHT_COLOR),
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
      // DataCell(
      //   Row(
      //     children: [
      //       Padding(
      //         padding: const EdgeInsets.symmetric(horizontal: 0),
      //         child: Text(
      //           fileInfo.firstName + fileInfo.lastName,
      //           style: TextStyle(color: DefultColors.LIGHT_COLOR),
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
    ],
  );
}
