import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/role_bloc.dart';
import 'package:whatever/bloc/role_events.dart';
import 'package:whatever/bloc/role_states.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/dashboard/responsive.dart';
import 'components/roles_files.dart';
import 'components/storage_details.dart';

class UserRoleScreen extends StatefulWidget {
  const UserRoleScreen({Key key, this.id}) : super(key: key);
  final int id;

  @override
  _UserRoleScreenState createState() => _UserRoleScreenState();
}

class _UserRoleScreenState extends State<UserRoleScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void initState() {
    super.initState();

    BlocProvider.of<RoleBloc>(context).add(GetRole(id: widget.id.toString()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            SizedBox(height: defaultPadding),
            BlocBuilder<RoleBloc, RoleState>(
              builder: (context, state) {
                if (state is RoleLoadInProgress) {
                  return CircularProgressIndicator();
                }
                if (state is RoleListLoadSuccess) {
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 5,
                        child: Column(
                          children: [
                            SizedBox(height: defaultPadding),
                            UserRoleFiles(
                              length: state.roles.length,
                              roles: state.roles,
                            ),
                            SizedBox(height: 20),
                            if (Responsive.isMobile(context))
                              SizedBox(height: defaultPadding),
                            if (Responsive.isMobile(context)) StarageDetails(),
                          ],
                        ),
                      ),
                      if (!Responsive.isMobile(context))
                        SizedBox(width: defaultPadding),
                    ],
                  );
                }
                if (state is RolesLoadFailure) {
                  return Container(
                    child: Text('Something went wrong'),
                    height: 30,
                    width: 40,
                    color: Colors.red,
                  );
                }

                return Container(
                  height: 20,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
