import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/roles_model.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';

class UserRoleFiles extends StatefulWidget {
  final List<RolesModel> roles;
  final int length;

  const UserRoleFiles({
    Key key,
    this.roles,
    this.length,
  }) : super(key: key);

  @override
  _UserRoleFilesState createState() => _UserRoleFilesState();
}

class _UserRoleFilesState extends State<UserRoleFiles> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        padding: EdgeInsets.all(defaultPadding),
        decoration: BoxDecoration(
          color: DefultColors.PRIMARY_DARK_COLOR,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text(
                'Roles',
                style: TextStyle(color: DefultColors.LIGHT_COLOR),
              ),
            ),
            SizedBox(
              height: 28,
            ),
            SizedBox(
              width: double.infinity,
              child: DataTable2(
                columnSpacing: defaultPadding,
                minWidth: 600,
                columns: [
                  DataColumn(
                    label: Text(
                      'Role Name',
                      style: TextStyle(color: DefultColors.LIGHT_COLOR),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Description',
                      style: TextStyle(color: DefultColors.LIGHT_COLOR),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Order',
                      style: TextStyle(color: DefultColors.LIGHT_COLOR),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Permission',
                      style: TextStyle(color: DefultColors.LIGHT_COLOR),
                    ),
                  )
                ],
                rows: List.generate(
                  widget.length,
                  (index) => userDataRow(widget.roles[index], context),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

DataRow userDataRow(RolesModel role, context) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Text(
                role.name,
                style: TextStyle(color: DefultColors.LIGHT_COLOR),
              ),
            ),
          ],
        ),
      ),
      DataCell(
        Text(
          role.description,
          style: TextStyle(color: DefultColors.LIGHT_COLOR),
        ),
      ),
      DataCell(
        Text(
          role.order.toString(),
          style: TextStyle(color: DefultColors.LIGHT_COLOR),
        ),
      ),
      DataCell(InkWell(
        onTap: () {
          // Navigator.of(context).push(
          //   MaterialPageRoute(
          //     builder: (context) => RolesPermissionsScreen(
          //       id: role.id,
          //     ),
          //   ),
          // );
        },
        child: Text(
          'Permissions',
          style: TextStyle(color: DefultColors.LIGHT_COLOR),
        ),
      ))
    ],
  );
}
