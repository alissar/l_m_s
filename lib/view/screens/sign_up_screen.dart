import 'dart:typed_data';

import 'package:country_picker/country_picker.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/sign_up_bloc.dart';
import 'package:whatever/bloc/sign_up_event.dart';
import 'package:whatever/bloc/sign_up_state.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/profile_screen.dart';
import 'package:whatever/view/widgets/title_textfeild_section.dart';

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  Uint8List _pickedImage;

  String _languageChosenValue;
  String _themeChosenValue;
  bool _obsecureText = true;
  void _togglevisibility() {
    setState(() {
      _obsecureText = !_obsecureText;
    });
  }

  TextEditingController firstNameEditingController = TextEditingController();
  TextEditingController middleNameEditingController = TextEditingController();
  TextEditingController lastNameEditingController = TextEditingController();
  TextEditingController nicknameEditingController = TextEditingController();
  TextEditingController emailEditingController = TextEditingController();
  TextEditingController passwordEditingController = TextEditingController();
  TextEditingController phoneEditingController = TextEditingController();
  TextEditingController phone2EditingController = TextEditingController();
  TextEditingController addressEditingController = TextEditingController();
  TextEditingController cityEditingController = TextEditingController();
  TextEditingController instEditingController = TextEditingController();
  TextEditingController depEditingController = TextEditingController();
  TextEditingController countryTextEditingController = TextEditingController();

  String country;

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  @override
  Widget build(BuildContext context) {
    final double widthSize = MediaQuery.of(context).size.width;
    final double heightSize = MediaQuery.of(context).size.height;

    return Container(
      color: DefultColors.LOW_LIGHT_COLOR,
      child: Center(
        child: Container(
          height: heightSize,
          width: widthSize * 0.6,
          child: Card(
            child: Material(
              child: Stack(children: [
                Container(
                  width: widthSize,
                  height: heightSize,
                  child: Image(
                    image: AssetImage('assets/images/sign_up.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(flex: 6, child: Container()),
                    Expanded(
                      flex: 7,
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          children: [
                            Expanded(
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    SizedBox(
                                      child: Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.03,
                                      ),
                                    ),
                                    Container(
                                      child: Form(
                                        child: BlocBuilder<SignUpBloc,
                                                SignUpState>(
                                            builder: (context, state) {
                                          if (state is SignUpInProgress) {
                                            return CircularProgressIndicator();
                                          }
                                          if (state is SignUpSuccess) {
                                            _onWidgetDidBuild(() {
                                              Navigator.pushReplacement(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ProfileScreen()));
                                            });

                                            return Container();
                                          }
                                          if (state is SignUpFailure) {
                                            return Center(
                                              child: Text(
                                                  'Failed: ' + state.error),
                                            );
                                          }
                                          if (state is SignUpInitial) {
                                            return Column(
                                              children: [
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      firstNameEditingController,
                                                  obsecureText: false,
                                                  title: 'First Name',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      middleNameEditingController,
                                                  obsecureText: false,
                                                  title: 'Middle Name',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      lastNameEditingController,
                                                  obsecureText: false,
                                                  title: 'Last Name',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      nicknameEditingController,
                                                  obsecureText: false,
                                                  title: 'Nickname',
                                                ),

                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      emailEditingController,
                                                  obsecureText: false,
                                                  title: 'Email',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      passwordEditingController,
                                                  obsecureText: _obsecureText,
                                                  title: 'Password',
                                                  suffixIcon: IconButton(
                                                    onPressed: () {
                                                      _togglevisibility();
                                                    },
                                                    icon: Icon(
                                                        _obsecureText
                                                            ? Icons
                                                                .visibility_off
                                                            : Icons.visibility,
                                                        color: DefultColors
                                                            .PRIMARY_RESET_COLOR),
                                                  ),
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      phoneEditingController,
                                                  obsecureText: false,
                                                  title: 'Phone',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      phone2EditingController,
                                                  obsecureText: false,
                                                  title: 'Phone2',
                                                ),

                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      addressEditingController,
                                                  obsecureText: false,
                                                  title: 'Address',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      countryTextEditingController,
                                                  suffixIcon: IconButton(
                                                    icon: Icon(
                                                      Icons
                                                          .arrow_drop_down_sharp,
                                                      color: DefultColors
                                                          .SECONDARY_RESET_COLOR,
                                                    ),
                                                    onPressed: () {
                                                      showCountryPicker(
                                                          context: context,
                                                          onSelect: (Country
                                                              pickedCountry) {
                                                            countryTextEditingController
                                                                    .text =
                                                                pickedCountry
                                                                    .displayNameNoCountryCode;
                                                            country = pickedCountry
                                                                .displayNameNoCountryCode;
                                                          });
                                                    },
                                                  ),
                                                  obsecureText: false,
                                                  title: 'Country',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      cityEditingController,
                                                  obsecureText: false,
                                                  title: 'City',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      instEditingController,
                                                  obsecureText: false,
                                                  title: 'Institution',
                                                ),
                                                TitleTextFeildSectionClass(
                                                  controller:
                                                      depEditingController,
                                                  obsecureText: false,
                                                  title: 'Department',
                                                ),

                                                ///TODO check items background color.
                                                Container(
                                                  padding: EdgeInsets.all(20),
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.18,
                                                  child: DropdownButton<String>(
                                                    isExpanded: true,
                                                    icon: Icon(
                                                      Icons.arrow_downward,
                                                      color: DefultColors
                                                          .PRIMARY_RESET_COLOR,
                                                    ),
                                                    focusColor: DefultColors
                                                        .PRIMARY_DARK_COLOR,
                                                    // dropdownColor:
                                                    //     Colors.grey[350],
                                                    value: _languageChosenValue,
                                                    elevation: 5,
                                                    style: TextStyle(
                                                        color: DefultColors
                                                            .PRIMARY_RESET_COLOR),
                                                    items: <String>[
                                                      'English',
                                                      'Arabic',
                                                    ].map<
                                                            DropdownMenuItem<
                                                                String>>(
                                                        (String value) {
                                                      return DropdownMenuItem(
                                                          value: value,
                                                          child: Text(value));
                                                    }).toList(),
                                                    hint: Text(
                                                      'Choose a lnguage',
                                                      style: TextStyle(
                                                          color: DefultColors
                                                              .PRIMARY_DARK_COLOR),
                                                    ),
                                                    onChanged: (String value) {
                                                      setState(() {
                                                        _languageChosenValue =
                                                            value;
                                                      });
                                                    },
                                                  ),
                                                ),

                                                Container(
                                                  padding: EdgeInsets.all(20),
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.18,
                                                  child: DropdownButton<String>(
                                                    isExpanded: true,

                                                    icon: Icon(
                                                      Icons.arrow_downward,
                                                      color: DefultColors
                                                          .PRIMARY_RESET_COLOR,
                                                    ),
                                                    focusColor: DefultColors
                                                        .PRIMARY_DARK_COLOR,
                                                    // dropdownColor:
                                                    //     Colors.grey[350],
                                                    value: _themeChosenValue,
                                                    // elevation: 5,

                                                    style: TextStyle(
                                                        color: DefultColors
                                                            .PRIMARY_RESET_COLOR),
                                                    items: <String>[
                                                      'Dark',
                                                      'Light',
                                                    ].map<
                                                            DropdownMenuItem<
                                                                String>>(
                                                        (String value) {
                                                      return DropdownMenuItem(
                                                          value: value,
                                                          child: Text(value));
                                                    }).toList(),
                                                    hint: Text(
                                                      'Choose a Theme',
                                                      style: TextStyle(
                                                          color: DefultColors
                                                              .PRIMARY_DARK_COLOR),
                                                    ),
                                                    onChanged: (String value) {
                                                      setState(() {
                                                        _themeChosenValue =
                                                            value;
                                                      });
                                                    },
                                                  ),
                                                ),
                                                Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  padding: EdgeInsets.fromLTRB(
                                                      117, 20, 0, 20),
                                                  child: Text(
                                                    'Choose Profile Pic',
                                                    style: TextStyle(
                                                        color: DefultColors
                                                            .PRIMARY_DARK_COLOR,
                                                        fontSize: DefultFontSize
                                                            .VERY_SMALL_FONT_SIZE),
                                                  ),
                                                ),
                                                Container(
                                                  child: GestureDetector(
                                                    child: Container(
                                                      child: Stack(
                                                        alignment:
                                                            Alignment.center,
                                                        children: [
                                                          Container(
                                                            width: widthSize *
                                                                0.09,
                                                            height: heightSize *
                                                                0.15,
                                                            decoration:
                                                                BoxDecoration(
                                                              border: Border.all(
                                                                  color: DefultColors
                                                                      .PRIMARY_DARK_COLOR),
                                                            ),
                                                          ),
                                                          Container(
                                                            width: widthSize *
                                                                0.09,
                                                            height: heightSize *
                                                                0.15,
                                                            child: Icon(
                                                              Icons.add,
                                                              color: DefultColors
                                                                  .PRIMARY_RESET_COLOR,
                                                              size: 32,
                                                            ),
                                                          ),
                                                          _pickedImage == null
                                                              ? Container()
                                                              : Container(
                                                                  width:
                                                                      widthSize *
                                                                          0.09,
                                                                  height:
                                                                      heightSize *
                                                                          0.15,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                          image:
                                                                              DecorationImage(
                                                                    image: MemoryImage(
                                                                        _pickedImage),
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  )),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                    onTap: () async {
                                                      FilePickerResult result =
                                                          await FilePicker
                                                              .platform
                                                              .pickFiles(
                                                                  type: FileType
                                                                      .custom,
                                                                  allowedExtensions: [
                                                            'jpg',
                                                            'png'
                                                          ]);
                                                      if (result != null) {
                                                        _pickedImage = result
                                                            .files.first.bytes;
                                                        setState(() {});
                                                      }
                                                    },
                                                  ),
                                                ),
                                                SizedBox(
                                                  child: Container(
                                                    height: 20,
                                                  ),
                                                ),
                                                ElevatedButton(
                                                  child: Text('Sign Up'),
                                                  onPressed: () async {
                                                    BlocProvider.of<SignUpBloc>(
                                                            context)
                                                        .add(SignUpRequested(
                                                      firstName:
                                                          firstNameEditingController
                                                              .text,
                                                      middleName:
                                                          middleNameEditingController
                                                              .text,
                                                      lastName:
                                                          lastNameEditingController
                                                              .text,
                                                      nickname:
                                                          nicknameEditingController
                                                              .text,
                                                      email:
                                                          emailEditingController
                                                              .text,
                                                      password:
                                                          passwordEditingController
                                                              .text,
                                                      phone:
                                                          phoneEditingController
                                                              .text,
                                                      phone2:
                                                          phone2EditingController
                                                              .text,
                                                      address:
                                                          addressEditingController
                                                              .text,
                                                      institution:
                                                          instEditingController
                                                              .text,
                                                      department:
                                                          depEditingController
                                                              .text,
                                                      city:
                                                          cityEditingController
                                                              .text,
                                                      country:
                                                          countryTextEditingController
                                                              .text,
                                                      imagePath: _pickedImage,
                                                    ));
                                                  },
                                                  style:
                                                      ElevatedButton.styleFrom(
                                                    primary: DefultColors
                                                        .PRIMARY_RESET_COLOR,
                                                  ),
                                                ),
                                                SizedBox(
                                                  child: Container(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.03,
                                                  ),
                                                ),
                                              ],
                                            );
                                          }
                                          return Container(
                                            height: 10,
                                          );
                                        }),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Positioned(
                  top: 28,
                  left: 125,
                  height: 150,
                  width: 150,
                  child: Container(
                    child: Text(
                      'Sign up',
                      style: TextStyle(
                          color: DefultColors.LOW_LIGHT_COLOR,
                          fontWeight: DefultFontWeight.BOLD,
                          fontSize: DefultFontSize.Very_LARGE_FONT_SIZE,
                          fontFamily: 'Roboto'),
                    ),
                  ),
                ),
                Positioned(
                  top: 82,
                  left: 100,
                  height: 150,
                  width: 200,
                  child: Text(
                    'Build skills with courses,\ncertificates, and degrees\n               online',
                    style: TextStyle(
                        color: DefultColors.LOW_LIGHT_COLOR,
                        fontSize: DefultFontSize.SMALL_FONT_SIZE,
                        fontWeight: DefultFontWeight.THIN,
                        fontFamily: 'Roboto'),
                  ),
                )
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
