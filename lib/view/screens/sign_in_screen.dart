import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/sign_in_bloc.dart';
import 'package:whatever/bloc/sign_in_event.dart';
import 'package:whatever/bloc/sign_in_state.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/forget_password.dart';
import 'package:whatever/view/screens/profile_screen.dart';
import 'package:whatever/view/screens/sign_up_screen.dart';
import 'package:whatever/view/widgets/costum_text_feild_widget.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _obsecureText = true;
  void _togglevisibility() {
    setState(() {
      _obsecureText = !_obsecureText;
    });
  }

  TextEditingController emailEditingController = TextEditingController();
  TextEditingController passwordEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final double widthSize = MediaQuery.of(context).size.width;
    final double heightSize = MediaQuery.of(context).size.height;
    return Container(
      color: DefultColors.LOW_LIGHT_COLOR,
      child: Center(
        child: Container(
          height: heightSize * 0.65,
          width: widthSize * 0.7,
          child: Card(
            elevation: 5,
            child: Row(children: [
              Expanded(
                flex: 5,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/signIn.jpg'),
                        fit: BoxFit.fill),
                  ),
                ),
              ),
              Expanded(
                  flex: 3,
                  child: Container(
                    child: Column(
                      children: [
                        SizedBox(
                          child: Container(
                            height: 30,
                          ),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            alignment: Alignment.center,
                            child: Text(
                              'Log In',
                              style: TextStyle(
                                  color: DefultColors.PRIMARY_RESET_COLOR,
                                  fontWeight: DefultFontWeight.BOLD,
                                  fontSize: DefultFontSize.LARGE_FONT_SIZE),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.2,
                            child: Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  Expanded(
                                    child: CostumTextFeildWidget(
                                      controller: emailEditingController,
                                      hint: 'E-mail',
                                      obsecureText: !_obsecureText,
                                    ),
                                  ),
                                  Expanded(
                                      child: CostumTextFeildWidget(
                                    controller: passwordEditingController,
                                    hint: 'Password',
                                    obsecureText: _obsecureText,
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        _togglevisibility();
                                      },
                                      icon: Icon(
                                          _obsecureText
                                              ? Icons.visibility_off
                                              : Icons.visibility,
                                          color:
                                              DefultColors.PRIMARY_RESET_COLOR),
                                    ),
                                  )),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          child: TextButton(
                            child: Text(
                              'Forget Your Password',
                              style: TextStyle(
                                  decoration: TextDecoration.underline),
                            ),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      ForgetPasswordScreen()));
                            },
                          ),
                        ),
                        Expanded(child: Container()),
                        BlocBuilder<SignInBloc, SignInState>(
                            builder: (context, state) {
                          if (state is SignInInitial) {
                            return ElevatedButton(
                              child: Text('   Sign In   '),
                              onPressed: () async {
                                if (!_formKey.currentState.validate()) {
                                  return;
                                }

                                BlocProvider.of<SignInBloc>(context).add(
                                    SignInRequested(
                                        email: emailEditingController.text,
                                        password:
                                            passwordEditingController.text));
                              },
                              style: ElevatedButton.styleFrom(
                                primary: DefultColors.PRIMARY_RESET_COLOR,
                              ),
                            );
                          }
                          if (state is SignInInProgress) {
                            return CircularProgressIndicator();
                          }
                          if (state is SignInSuccess) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfileScreen()));
                            return Container(
                              width: 10,
                              height: 10,
                            );
                          }

                          if (state is SignInFailure) {
                            return Container(
                              height: 40,
                              color: Colors.red,
                              child: Text('logIn failed'),
                            );
                          }

                          return Container(
                            width: 10,
                            height: 10,
                          );
                        }),
                        Expanded(child: Container()),
                        Container(
                          child: Text('Dont have an account !'),
                        ),
                        Container(
                          child: TextButton(
                            child: Text(
                              ' Sign Up Now ',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                              ),
                            ),
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => SignUpScreen(),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  )),
            ]),
          ),
        ),
      ),
    );
  }
}
