import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:whatever/bloc/reset_password_req_bloc.dart';
import 'package:whatever/bloc/reset_password_req_event.dart';
import 'package:whatever/bloc/reset_password_req_state.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/sign_in_screen.dart';
import 'package:whatever/view/widgets/costum_text_feild_widget.dart';

class ForgetPasswordScreen extends StatefulWidget {
  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _obsecureText = true;

  TextEditingController emailEditingController = TextEditingController();

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  @override
  Widget build(BuildContext context) {
    final double widthSize = MediaQuery.of(context).size.width;
    final double heightSize = MediaQuery.of(context).size.height;
    return Container(
      color: DefultColors.LOW_LIGHT_COLOR,
      child: Center(
        child: Container(
          height: heightSize * 0.65,
          width: widthSize * 0.7,
          child: Card(
            elevation: 5,
            child: Row(children: [
              Expanded(
                flex: 5,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/signIn.jpg'),
                        fit: BoxFit.fill),
                  ),
                ),
              ),
              Expanded(
                  flex: 3,
                  child: Container(
                    child: Column(
                      children: [
                        SizedBox(
                          child: Container(
                            height: 30,
                          ),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            alignment: Alignment.center,
                            child: Text(
                              'Reset Your Password',
                              style: TextStyle(
                                  color: DefultColors.PRIMARY_RESET_COLOR,
                                  fontWeight: DefultFontWeight.BOLD,
                                  fontSize: DefultFontSize.MEDIUM_FONT_SIZE),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.2,
                            child: Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  Expanded(
                                    child: CostumTextFeildWidget(
                                      controller: emailEditingController,
                                      hint: 'E-mail',
                                      obsecureText: !_obsecureText,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(child: Container()),
                        BlocBuilder<ResetPasswordReqBloc,
                            ResetPasswordReqState>(builder: (context, state) {
                          if (state is ResetPasswordReqinitialState) {
                            return ElevatedButton(
                              child: Text('   Submit  '),
                              onPressed: () async {
                                if (!_formKey.currentState.validate()) {
                                  return;
                                }
                                BlocProvider.of<ResetPasswordReqBloc>(context)
                                    .add(ResetPasswordReq(
                                  email: emailEditingController.text,
                                ));
                              },
                              style: ElevatedButton.styleFrom(
                                primary: DefultColors.PRIMARY_RESET_COLOR,
                              ),
                            );
                          }
                          if (state is ResetPasswordReqLoadInProgress) {
                            return CircularProgressIndicator();
                          }
                          if (state is ResetPasswordReqLoadSuccess) {
                            _onWidgetDidBuild(() {
                              Fluttertoast.showToast(
                                msg:
                                    'A password-reset link has been sent to your email',
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.BOTTOM,
                                backgroundColor:
                                    DefultColors.SECONDARY_LIGHT_COLOR,
                                textColor: DefultColors.LIGHT_COLOR,
                                fontSize: (16.0),
                              );

                              BlocProvider.of<ResetPasswordReqBloc>(context)
                                  .add(Reset());

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignInScreen()));
                              return Container(
                                width: 10,
                                height: 10,
                              );
                            });
                          }

                          if (state is ResetPasswordReqLoadInFailure) {
                            return Container(
                              height: 40,
                              color: Colors.red,
                              child: Text('logIn failed: ' + state.error),
                            );
                          }

                          return Container(
                            width: 30,
                            height: 30,
                          );
                        }),
                        Expanded(child: Container()),
                      ],
                    ),
                  )),
            ]),
          ),
        ),
      ),
    );
  }
}
