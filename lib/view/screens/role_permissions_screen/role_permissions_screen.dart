import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/role_bloc.dart';
import 'package:whatever/bloc/role_events.dart';
import 'package:whatever/bloc/role_states.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/dashboard/responsive.dart';
import 'components/role_permissions_files.dart';

class RolePermissionsScreen extends StatefulWidget {
  final int roleId;
  final int page = 1;

  const RolePermissionsScreen({Key key, this.roleId}) : super(key: key);

  @override
  _RolePermissionsScreenState createState() => _RolePermissionsScreenState();
}

class _RolePermissionsScreenState extends State<RolePermissionsScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void initState() {
    super.initState();

    BlocProvider.of<RoleBloc>(context).add(
      GetRolePermissions(
        roleId: widget.roleId,
        page: widget.page,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            SizedBox(height: defaultPadding),
            BlocBuilder<RoleBloc, RoleState>(builder: (context, state) {
              if (state is RoleLoadInProgress) {
                return CircularProgressIndicator();
              }
              if (state is RolePermissionsLoadSuccess) {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          SizedBox(height: defaultPadding),
                          RolePermissionsFiles(
                            roleId: widget.roleId,
                            subTitle0: 'Name',
                            subTitle1: 'Description',
                            subTitle2: 'ID',
                            subTitle3: 'Remove Permissions',
                            title: 'Role Permissions',
                            length: state.permissions.length,
                            rolePermissions: state.permissions,
                          ),
                          SizedBox(height: 20),
                          if (Responsive.isMobile(context))
                            SizedBox(height: defaultPadding),
                        ],
                      ),
                    ),
                    if (!Responsive.isMobile(context))
                      SizedBox(width: defaultPadding),
                  ],
                );
              }
              if (state is RolesLoadFailure) {
                return Container(
                  child: Text('Something went wrong'),
                  height: 50,
                  width: 200,
                  color: Colors.red,
                );
              }

              return Container(
                height: 20,
              );
            }),
          ],
        ),
      ),
    );
  }
}
