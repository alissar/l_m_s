import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/permissions_event.dart';
import 'package:whatever/bloc/permissions_state.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/role_permissions_screen/components/add_permission_to_role_popup.dart';
import 'package:whatever/view/screens/role_permissions_screen/components/remove_permission_from_role_popup.dart';

class RolePermissionsFiles extends StatefulWidget {
  final int roleId;
  final List<PermissionsModel> rolePermissions;
  final int length;
  final String title;
  final String subTitle0;
  final String subTitle1;
  final String subTitle2;
  final String subTitle3;

  const RolePermissionsFiles({
    Key key,
    this.rolePermissions,
    this.length,
    this.title,
    this.subTitle0,
    this.subTitle1,
    this.subTitle2,
    this.subTitle3,
    this.roleId,
  }) : super(key: key);

  @override
  _RolePermissionsFilesState createState() => _RolePermissionsFilesState();
}

class _RolePermissionsFilesState extends State<RolePermissionsFiles> {
  List<PermissionsModel> _permissions;

  @override
  void initState() {
    super.initState();

    _permissions = widget.rolePermissions;
    BlocProvider.of<PermissionsBloc>(context).add(AllPermissionsRequested());
  }

  int val;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(defaultPadding),
            decoration: BoxDecoration(
              color: DefultColors.PRIMARY_DARK_COLOR,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Text(widget.title,
                      style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                ),
                SizedBox(height: 28),
                SizedBox(
                  width: double.infinity,
                  child: DataTable2(
                    columnSpacing: defaultPadding,
                    minWidth: 600,
                    columns: [
                      DataColumn(
                        label: Text(
                          widget.subTitle0,
                          style: TextStyle(color: DefultColors.LIGHT_COLOR),
                        ),
                      ),
                      DataColumn(
                        label: Text(widget.subTitle1,
                            style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                      ),
                      DataColumn(
                        label: Text(widget.subTitle2,
                            style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                      ),
                      DataColumn(
                        label: Text(widget.subTitle3,
                            style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                      )
                    ],
                    rows: List.generate(
                      _permissions.length,
                      (index) => userDataRow(
                          _permissions[index], context, widget.roleId),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 28,
          ),
          BlocBuilder<PermissionsBloc, PermissionsState>(
            builder: (context, state) {
              if (state is PermissionsLoadInProgress) {
                return CircularProgressIndicator();
              }
              if (state is PermissionsLoadSuccess) {
                return Container(
                  height: 35,
                  width: 100,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: DefultColors.PRIMARY_DARK_COLOR),
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (context) => AddPermissionsToRolePopup(
                                roleId: widget.roleId,
                                allPermissions: state.permissions,
                              ));
                    },
                    child: Text('Add Permission To Role'),
                  ),
                );
              }
              if (state is PermissionsLoadFailure) {
                return Center(
                  child: Text(state.error),
                );
              }
              return Container();
            },
          ),
        ],
      ),
    );
  }
}

DataRow userDataRow(PermissionsModel permissionModel, context, int roleId) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Text(
                permissionModel.perName,
                style: TextStyle(color: DefultColors.LIGHT_COLOR),
              ),
            ),
          ],
        ),
      ),
      DataCell(
        Text(
          permissionModel.perDescription,
          style: TextStyle(color: DefultColors.LIGHT_COLOR),
        ),
      ),
      DataCell(
        InkWell(
          child: Text(
            permissionModel.id.toString(),
            style: TextStyle(color: DefultColors.LIGHT_COLOR),
          ),
        ),
      ),
      DataCell(
        InkWell(
          onTap: () {
            showDialog(
                context: context,
                builder: (context) => RemovePermissionsFromRolePopup(
                      roleId: roleId,
                      permissionId: permissionModel.id,
                    ));
          },
          child: Text(
            'Remove Permission',
            style: TextStyle(color: DefultColors.LIGHT_COLOR),
          ),
        ),
      ),
    ],
  );
}
