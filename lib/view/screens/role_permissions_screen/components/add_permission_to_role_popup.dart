import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/permissions_event.dart';
import 'package:whatever/bloc/role_bloc.dart';
import 'package:whatever/bloc/role_events.dart';
import 'package:whatever/bloc/role_states.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/permissions_model.dart';

class AddPermissionsToRolePopup extends StatefulWidget {
  final List<PermissionsModel> allPermissions;
  final int roleId;

  const AddPermissionsToRolePopup({
    Key key,
    this.roleId,
    this.allPermissions,
  }) : super(key: key);

  @override
  _AddPermissionsToRolePopupState createState() =>
      _AddPermissionsToRolePopupState();
}

class _AddPermissionsToRolePopupState extends State<AddPermissionsToRolePopup> {
  int val;

  List<DropdownMenuItem> _layoutDropdownItems(
      List<PermissionsModel> allPermissions) {
    List<DropdownMenuItem> dropdownMenuItems = [];

    for (PermissionsModel permission in allPermissions) {
      dropdownMenuItems.add(
        DropdownMenuItem(
          child: Text(permission.perName),
          value: permission.id,
        ),
      );
    }

    return dropdownMenuItems;
  }

  @override
  void initState() {
    super.initState();

    BlocProvider.of<RoleBloc>(context)
        .add(GetRolePermissions(roleId: widget.roleId, page: 1));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Container(
          height: 200,
          width: 300,
          decoration: BoxDecoration(
              color: DefultColors.LIGHT_COLOR,
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Text('Add Permission', style: TextStyle(fontSize: 25)),
              SizedBox(
                height: 10,
              ),
              Text('Select which permission to add',
                  style: TextStyle(fontSize: 15), textAlign: TextAlign.center),
              SizedBox(
                height: 10,
              ),
              BlocBuilder<RoleBloc, RoleState>(
                builder: (context, state) {
                  if (state is RoleLoadInProgress) {
                    return CircularProgressIndicator();
                  }
                  if (state is RolePermissionsLoadSuccess) {
                    return Container(
                      height: 110,
                      width: 150,
                      child: Column(
                        children: [
                          DropdownButton(
                            value: val,
                            onChanged: (value) {
                              val = value;
                            },
                            items: _layoutDropdownItems(widget.allPermissions),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    BlocProvider.of<PermissionsBloc>(context)
                                        .add(AddPermissionToRole(
                                            pId: [val],
                                            id: widget.roleId.toString()));
                                    Navigator.pop(context);
                                  },
                                  child: Text('Confirm')),
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text('Cancel')),
                            ],
                          ),
                        ],
                      ),
                    );
                  }
                  return Container();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
