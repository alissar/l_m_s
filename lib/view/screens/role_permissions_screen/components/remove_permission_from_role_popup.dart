import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/permissions_event.dart';
import 'package:whatever/bloc/role_bloc.dart';
import 'package:whatever/common/settings.dart';

class RemovePermissionsFromRolePopup extends StatelessWidget {
  final int roleId;
  final int permissionId;

  const RemovePermissionsFromRolePopup({
    Key key,
    this.roleId,
    this.permissionId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 150,
        width: 300,
        decoration: BoxDecoration(
            color: DefultColors.LIGHT_COLOR,
            borderRadius: BorderRadius.circular(10)),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Text(
              'Remove Permission',
              style: TextStyle(fontSize: 25),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Are you sure you want to remove this permission from this role?',
              style: TextStyle(fontSize: 15),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                    onPressed: () {
                      BlocProvider.of<PermissionsBloc>(context).add(
                          RemovePermissionsFromRole(
                              pId: [permissionId], id: roleId));
                      Navigator.pop(context);
                    },
                    child: Text('Confirm')),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Cancel')),
              ],
            )
          ],
        ),
      ),
    );
  }
}
