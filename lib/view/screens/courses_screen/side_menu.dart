import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/dashboard/dashboard_screen.dart';
import 'package:whatever/view/screens/users_screen/users_screen.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Center(
              child: Text(
                'L_M_S',
                style: TextStyle(
                    color: DefultColors.LOW_YELLOW_COLOR,
                    fontWeight: DefultFontWeight.BOLD,
                    fontSize: DefultFontSize.MEDIUM_FONT_SIZE),
              ),
            ),
          ),
          DrawerListTile(
            title: "Dashboard",
            svgSrc: "assets/icons/menu_dashbord.svg",
            press: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => DashboardScreen()));
            },
          ),
          DrawerListTile(
            title: "Users",
            svgSrc: "assets/icons/menu_tran.svg",
            press: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => UsersScreen()));
            },
          ),
          DrawerListTile(
            title: "Permissions",
            svgSrc: "assets/icons/menu_task.svg",
            press: () {},
          ),
          DrawerListTile(
            title: "Roles",
            svgSrc: "assets/icons/menu_doc.svg",
            press: () {},
          ),
          DrawerListTile(
            title: "Courses",
            svgSrc: "assets/icons/menu_store.svg",
            press: () {},
          ),
          DrawerListTile(
            title: "Assignments",
            svgSrc: "assets/icons/menu_notification.svg",
            press: () {},
          ),
          DrawerListTile(
            title: "Settings",
            svgSrc: "assets/icons/menu_setting.svg",
            press: () {},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key key,
    // For selecting those three line once press "Command+D"
    @required this.title,
    @required this.svgSrc,
    @required this.press,
  }) : super(key: key);

  final String title, svgSrc;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      leading: SvgPicture.asset(
        svgSrc,
        color: DefultColors.PRIMARY_LIGHT_COLOR,
        height: 16,
      ),
      title: Text(
        title,
        style: TextStyle(
            color: DefultColors.PRIMARY_DARK_COLOR,
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
