import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';

class StorageInfoCard extends StatelessWidget {
  const StorageInfoCard({
    Key key,
    @required this.title,
    @required this.svgSrc,
    @required this.amountOfFiles,
    @required this.numOfFiles,
  }) : super(key: key);

  final String title, svgSrc, amountOfFiles;
  final int numOfFiles;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 35),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        border: Border.all(width: 2, color: primaryColor.withOpacity(0.15)),
        borderRadius: const BorderRadius.all(
          Radius.circular(defaultPadding),
        ),
      ),
      child: Row(
        children: [
          SizedBox(
            height: 20,
            width: 20,
            child: SvgPicture.asset(svgSrc),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(color: DefultColors.LIGHT_COLOR),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text("$numOfFiles Files",
                      style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                ],
              ),
            ),
          ),
          Text(
            amountOfFiles,
            style: TextStyle(color: DefultColors.LIGHT_COLOR),
          )
        ],
      ),
    );
  }
}
