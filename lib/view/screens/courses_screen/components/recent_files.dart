import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/dashboard/RecentFile.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';

class RecentFiles extends StatelessWidget {
  const RecentFiles({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: DefultColors.PRIMARY_DARK_COLOR,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: AlignmentDirectional.center,
            child: Text("Recent Courses",
                style: TextStyle(
                  color: DefultColors.LIGHT_COLOR,
                  fontSize: DefultFontSize.SMALL_FONT_SIZE,
                )),
          ),
          SizedBox(
            width: double.infinity,
            child: DataTable2(
              columnSpacing: defaultPadding,
              minWidth: 600,
              columns: [
                DataColumn(
                  label: Text(
                    "Course",
                    style: TextStyle(
                      color: DefultColors.LIGHT_COLOR,
                      fontSize: DefultFontSize.SMALL_FONT_SIZE,
                    ),
                  ),
                ),
                DataColumn(
                  label: Text(
                    "Start Date",
                    style: TextStyle(
                      color: DefultColors.LIGHT_COLOR,
                      fontSize: DefultFontSize.SMALL_FONT_SIZE,
                    ),
                  ),
                ),
                DataColumn(
                  label: Text(
                    "End Date",
                    style: TextStyle(
                      color: DefultColors.LIGHT_COLOR,
                      fontSize: DefultFontSize.SMALL_FONT_SIZE,
                    ),
                  ),
                ),
              ],
              rows: List.generate(
                demoRecentFiles.length,
                (index) => recentFileDataRow(demoRecentFiles[index]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

DataRow recentFileDataRow(RecentFile fileInfo) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0),
              child: Text(
                fileInfo.title,
                style: TextStyle(color: DefultColors.LIGHT_COLOR),
              ),
            ),
          ],
        ),
      ),
      DataCell(Text(
        fileInfo.date,
        style: TextStyle(color: DefultColors.LIGHT_COLOR),
      )),
      DataCell(Text(
        fileInfo.size,
        style: TextStyle(color: DefultColors.LIGHT_COLOR),
      )),
    ],
  );
}
