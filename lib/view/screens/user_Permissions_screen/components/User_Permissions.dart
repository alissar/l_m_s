import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/permissions_event.dart' as event;
import 'package:whatever/bloc/roles_bloc.dart';
import 'package:whatever/bloc/user_permissions_bloc.dart';
import 'package:whatever/bloc/user_permissions_event.dart';
import 'package:whatever/bloc/user_permissions_state.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';

class UserPermissionsFiles extends StatefulWidget {
  final String userId;
  final List<PermissionsModel> userPermissions;
  final int length;
  final String title;
  final String dataString;
  final String subTitle0;
  final String subTitle1;
  final String subTitle2;
  final String subTitle3;
  final bool deletedUSers;

  const UserPermissionsFiles({
    Key key,
    this.userPermissions,
    this.length,
    this.title,
    this.dataString,
    this.subTitle0,
    this.subTitle1,
    this.subTitle2,
    this.subTitle3,
    this.deletedUSers,
    this.userId,
  }) : super(key: key);

  @override
  _UserPermissionsFilesState createState() => _UserPermissionsFilesState();
}

class _UserPermissionsFilesState extends State<UserPermissionsFiles> {
  List<PermissionsModel> _permissions;

  void _addPermissionToUserPermissions(PermissionsModel permission) {
    setState(() {
      print(permission.id);
      _permissions.add(permission);
      print(_permissions.map((e) => e.id).toList());
      BlocProvider.of<UserPermissionsBloc>(context)
          .add(AllPermissionsRequested());
    });
  }

  void _removePermissionFromUserPermissions(PermissionsModel permissionsModel) {
    setState(() {
      print(permissionsModel.id);
      _permissions.removeWhere((element) => element.id == permissionsModel.id);
      print(_permissions.map((e) => e.id).toList());
      BlocProvider.of<UserPermissionsBloc>(context)
          .add(AllPermissionsRequested());
    });
  }

  List<DropdownMenuItem> _layoutDropdownItems(
      List<PermissionsModel> allPermissions,
      List<PermissionsModel> userPermissions) {
    List<DropdownMenuItem> dropdownMenuItems = [];

    for (PermissionsModel permission in allPermissions) {
      List<PermissionsModel> foundPermissions = userPermissions
          .where((element) => element.id == permission.id)
          .toList();
      if (foundPermissions.length == 0) {
        dropdownMenuItems.add(
          DropdownMenuItem(
            onTap: () {
              _addPermissionToUserPermissions(permission);
            },
            child: Text(permission.perName),
            value: permission.id,
          ),
        );
      }
    }

    return dropdownMenuItems;
  }

  @override
  void initState() {
    super.initState();

    _permissions = widget.userPermissions;
    BlocProvider.of<UserPermissionsBloc>(context)
        .add(AllPermissionsRequested());
  }

  int val;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(defaultPadding),
            decoration: BoxDecoration(
              color: DefultColors.PRIMARY_DARK_COLOR,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Text(widget.title,
                      style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                ),
                SizedBox(height: 28),
                SizedBox(
                  width: double.infinity,
                  child: DataTable2(
                    columnSpacing: defaultPadding,
                    minWidth: 600,
                    columns: [
                      DataColumn(
                        label: Text(
                          widget.subTitle0,
                          style: TextStyle(color: DefultColors.LIGHT_COLOR),
                        ),
                      ),
                      DataColumn(
                        label: Text(widget.subTitle1,
                            style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                      ),
                      DataColumn(
                        label: Text(widget.subTitle2,
                            style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                      ),
                      DataColumn(
                        label: Text(widget.subTitle3,
                            style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                      )
                    ],
                    rows: List.generate(
                      _permissions.length,
                      (index) => userDataRow(_permissions[index],
                          _removePermissionFromUserPermissions),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 28,
          ),
          BlocBuilder<UserPermissionsBloc, UserPermissionsState>(
            builder: (context, state) {
              if (state is UserPermissionsLoanInProgress) {
                return CircularProgressIndicator();
              }
              if (state is UserPermissionsLoadSuccess) {
                return Container(
                  height: 50,
                  width: 150,
                  child: DropdownButton(
                    value: val,
                    onChanged: (value) {},
                    items:
                        _layoutDropdownItems(state.permissions, _permissions),
                  ),
                );
              }
              return Container();
            },
          ),
          Container(
            height: 35,
            width: 100,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: DefultColors.PRIMARY_DARK_COLOR),
              onPressed: () {
                BlocProvider.of<PermissionsBloc>(context).add(
                    event.AddOrRemovePermissions(
                        userId: widget.userId,
                        permissions: _permissions.map((e) => e.id).toList()));
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}

DataRow userDataRow(
    PermissionsModel permissionModel, Function removeFromUserPermissions) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Text(
                permissionModel.perName,
                style: TextStyle(color: DefultColors.LIGHT_COLOR),
              ),
            ),
          ],
        ),
      ),
      DataCell(
        Text(
          permissionModel.perDescription,
          style: TextStyle(color: DefultColors.LIGHT_COLOR),
        ),
      ),
      DataCell(
        InkWell(
          child: Text(
            permissionModel.id.toString(),
            style: TextStyle(color: DefultColors.LIGHT_COLOR),
          ),
        ),
      ),
      DataCell(
        InkWell(
          onTap: () {
            removeFromUserPermissions(permissionModel);
          },
          child: Text(
            'Remove Permission',
            style: TextStyle(color: DefultColors.LIGHT_COLOR),
          ),
        ),
      ),
    ],
  );
}
