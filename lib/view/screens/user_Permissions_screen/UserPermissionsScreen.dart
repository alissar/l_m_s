import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/permissions_event.dart';
import 'package:whatever/bloc/permissions_state.dart';
import 'package:whatever/bloc/user_bloc.dart';
import 'package:whatever/bloc/user_events.dart';
import 'package:whatever/bloc/user_states.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/dashboard/responsive.dart';
import 'components/User_Permissions.dart';
import 'components/storage_details.dart';

class UserPermissionsScreen extends StatefulWidget {
  final int id;
  final int page = 1;

  const UserPermissionsScreen({Key key, this.id}) : super(key: key);

  @override
  _UserPermissionsScreenState createState() => _UserPermissionsScreenState();
}

class _UserPermissionsScreenState extends State<UserPermissionsScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void initState() {
    super.initState();

    BlocProvider.of<PermissionsBloc>(context).add(
      GetUserPermissions(
        userId: widget.id.toString(),
        page: widget.page,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            SizedBox(height: defaultPadding),
            BlocBuilder<PermissionsBloc, PermissionsState>(
                builder: (context, state) {
              if (state is PermissionsLoadInProgress) {
                return CircularProgressIndicator();
              }
              if (state is PermissionsLoadSuccess) {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          SizedBox(height: defaultPadding),
                          UserPermissionsFiles(
                            userId: widget.id.toString(),
                            subTitle0: 'Name',
                            subTitle1: 'Description',
                            subTitle2: 'ID',
                            subTitle3: 'Remove Permissions',
                            dataString: 'ID',
                            title: 'Users',
                            length: state.permissions.length,
                            userPermissions: state.permissions,
                          ),
                          SizedBox(height: 20),
                          if (Responsive.isMobile(context))
                            SizedBox(height: defaultPadding),
                          if (Responsive.isMobile(context)) StarageDetails(),
                        ],
                      ),
                    ),
                    if (!Responsive.isMobile(context))
                      SizedBox(width: defaultPadding),
                  ],
                );
              }
              if (state is PermissionsLoadFailure) {
                return Container(
                  child: Text('Something went wrong'),
                  height: 30,
                  width: 40,
                  color: Colors.red,
                );
              }

              return Container(
                height: 20,
              );
            }),
          ],
        ),
      ),
    );
  }
}
