import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/sign_in_screen.dart';
import 'package:whatever/view/screens/sign_up_screen.dart';
import 'package:whatever/view/widgets/rounded_button_widget.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth > 1200) {
            //For Desktop Screen
            return DesktopPage();
          } else if (constraints.maxWidth >= 800 &&
              constraints.maxWidth <= 1200) {
            //for Tablet Screen
            return DesktopPage();
          } else {
            //for mobile Screen
            return DesktopPage();
          }
        },
      ),
    );
  }
}

class DesktopPage extends StatefulWidget {
  @override
  _DesktopPageState createState() => _DesktopPageState();
}

class _DesktopPageState extends State<DesktopPage> {
  Widget customFlexible(String text, String subText, var icon) {
    return Flexible(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            CircleAvatar(
              backgroundColor: DefultColors.LOW_YELLOW_COLOR,
              radius: 32.0,
              child: Icon(icon, color: Colors.white, size: 28.0),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              text,
              style: TextThemes.createText,
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              subText,
              style: TextThemes.howitworkDec,
            ),
          ],
        ),
      ),
    );
  }

  Widget customCheckBox(String text, bool value) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(
              activeColor: DefultColors.LOW_YELLOW_COLOR,
              value: value,
              onChanged: (value) {}),
          SizedBox(
            width: 16.0,
          ),
          Text(
            text,
            style: TextThemes.smallWhiteText,
          ),
        ],
      ),
    );
  }

  Widget customCard(text, img) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0), color: Colors.teal),
          height: MediaQuery.of(context).size.height * 0.47,
          width: MediaQuery.of(context).size.width * 0.22,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(16.0),
            child: Image(
              fit: BoxFit.cover,
              image: AssetImage(img),
            ),
          ),
        ),
        SizedBox(
          height: 16.0,
        ),
        Text(
          text,
          style: TextThemes.cardText,
        )
      ],
    );
  }

  Widget customCircleAvtar(text, img, subText) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircleAvatar(
          radius: 100.0,
          backgroundImage: AssetImage(img),
        ),
        SizedBox(
          height: 16.0,
        ),
        Text(
          text,
          style: TextThemes.cardText,
        ),
        SizedBox(
          height: 16.0,
        ),
        Text(
          subText,
          style: TextThemes.whitexlText,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          //Home Music for everyone
          Stack(
            children: [
              Container(
                width: size.width,
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0),
                  child: Image(
                    fit: BoxFit.cover,
                    image: AssetImage("assets/images/landingBG.png"),
                  ),
                ),
              ),
              Positioned(
                width: size.width / 2,
                top: 150,
                left: 100,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: size.width * 0.9,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Learning ",
                                style: TextThemes.yellowTitle,
                              ),
                              Text(
                                "management",
                                style: TextThemes.homeDarkTitle,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "system",
                                style: TextThemes.homeDarkTitle,
                              ),
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 32.0),
                                child: Text(
                                  "Your way to unlimited learning trip",
                                  style: TextThemes.homeSubTitle,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 32.0, vertical: 164.0),
                      child: Row(
                        children: [
                          Container(
                            height: 32,
                            width: 100,
                            child: ElevatedButton(
                              child: Text('Sign in'),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SignInScreen()));
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: DefultColors.PRIMARY_DARK_COLOR),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            height: 32,
                            width: 100,
                            child: ElevatedButton(
                              child: Text('SignUp'),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SignUpScreen()));
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: DefultColors.PRIMARY_DARK_COLOR),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          //Unlimited Access
          Container(
            height: size.height * 0.74,
            width: size.width,
            color: Colors.white,
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center,
                  width: size.width / 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 26.0),
                    child: Wrap(
                      children: [
                        Text(
                          "We present for you ",
                          style: TextThemes.helpDarkText,
                        ),
                        Text(
                          "Unlimited Access to 100 Course",
                          style: TextThemes.bigTextTitle,
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: size.width / 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Wrap(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Text(
                              "Save Time & Money on Your LMS Software Search \nContact Us Today! Talk to Us, Read Reviews, Discover Authentic Research, So You Can Make the Right Decision. 770K Trusted User Reviews. 715K Buyers Advised. Free Personalized Process. 600+ Software Guides.",
                              style: TextThemes.descriptionText,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 32.0,
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                        height: 50,
                        width: 200,
                        child: ElevatedButton(
                          child: Text('Try it now'),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => SignUpScreen()));
                          },
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(32)),
                              primary: DefultColors.PRIMARY_DARK_COLOR),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),

          //How it works
          Container(
            height: size.height * 0.86,
            width: size.width,
            color: DefultColors.PRIMARY_DARK_COLOR,
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 32.0, vertical: 6.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: size.width * 0.46,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Text(
                        "How it works ?   ",
                        style: TextThemes.whititleText,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 48.0,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      customFlexible(
                        "Create an account",
                        "Sign Up for new account and Provide some information about you and your business to get us started.",
                        Icons.person_pin,
                      ),
                      customFlexible(
                        "Choose a plan",
                        "Read product summaries and user reviews on specific Learning Management Systems products.",
                        Icons.queue_play_next,
                      ),
                      customFlexible(
                        "Start Learning",
                        "Choose your classes, Specialization, Subjects, and your time to make the best learning plan for you ",
                        Icons.library_music,
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),

          //Our Concept
          Container(
            height: size.height * 1.20,
            width: size.width,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Flexible(
                            flex: 1,
                            child: Wrap(
                              children: [
                                Container(
                                  padding: EdgeInsets.only(left: 50),
                                  child: Text(
                                    "Our Goals :",
                                    style: TextThemes.purpuleText,
                                  ),
                                ),
                              ],
                            )),
                        Flexible(
                            flex: 3,
                            child: Wrap(
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 16.0),
                                  child: Text(
                                    "Help users organize and simplify training or learning administration, which includes processes such as distributing content, managing user information, scheduling, and overseeing course enrollment.",
                                    style: TextThemes.helpDarkText,
                                  ),
                                )
                              ],
                            ))
                      ],
                    )),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                              flex: 1,
                              child: customCard('Team Work', 'images/11.jpg')),
                          Flexible(
                              flex: 1,
                              child:
                                  customCard('Easy to reach', 'images/22.jpg')),
                          Flexible(
                              flex: 1,
                              child: customCard(
                                  'any where in the world', 'images/33.jpg')),
                          Flexible(
                              flex: 1,
                              child:
                                  customCard('Self Develop', 'images/44.jpg')),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          //Subscription
          Container(
            alignment: Alignment.center,
            height: size.height,
            width: size.width,
            color: DefultColors.PRIMARY_DARK_COLOR,
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 32.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Wrap(
                              children: [
                                Text(
                                  "What We do",
                                  style: TextThemes.whititleText,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 18.0,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Wrap(
                              children: [
                                Text(
                                  "LEARNING AVAILABLE ON ANY DEVICE",
                                  style: TextThemes.pinkText,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 18.0,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Wrap(
                              children: [
                                Text(
                                  "Today's employees expect a personalized learning experience, available in the palm of their hands with an intuitive and engaging user experience. Relevant learning served to any mobile and tablet OS helps increase their overall engagement and productivity – ensuring they have a seamless connection to courses and content right in the flow of work. The LMS puts continuous learning within reach for both online and offline learning.",
                                  style: TextThemes.descriptionText,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                            height: 50,
                            width: 200,
                            child: ElevatedButton(
                              child: Text('Try it now'),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SignUpScreen()));
                              },
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(32)),
                                  primary: DefultColors.LOW_YELLOW_COLOR),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 60.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                      color: DefultColors.LOW_LIGHT_COLOR,
                      child: Container(
                        alignment: Alignment.center,
                        height: MediaQuery.of(context).size.height * 0.65,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 32.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              customCheckBox('Play any track', true),
                              customCheckBox('Listen offline', true),
                              customCheckBox('No ad interruptions', true),
                              customCheckBox('Unlimited skips', true),
                              customCheckBox('High quality audio', true),
                              customCheckBox('Shuffle play', true),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          //People
          Container(
            height: size.height * 0.65,
            width: size.width,
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                customCircleAvtar("No ad interruptions", "images/pic6.jfif",
                    "Consectetur adipiscing elit"),
                customCircleAvtar("High Quality", "images/pic7.jfif",
                    "Ectetur adipiscing elit"),
                customCircleAvtar("Listen Offline", "images/pic8.jfif",
                    "Sed do eiusmod tempor"),
                customCircleAvtar(
                    "Download Music", "images/pic9.jfif", "Adipiscing elit")
              ],
            ),
          ),

          //Footer
          Container(
            height: size.height * 0.60,
            width: size.width,
            color: DefultColors.PRIMARY_DARK_COLOR,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 120),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          footerText("FAQ"),
                          footerText("Investor Relations"),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          footerText("Help Centre"),
                          footerText("Legal Notices"),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          footerText("Account"),
                          footerText("Corporate Information"),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          footerText("Info Centre"),
                          footerText("Contact Us"),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget footerText(text) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Text(
        text,
        style: TextThemes.footerText,
      ),
    );
  }
}
