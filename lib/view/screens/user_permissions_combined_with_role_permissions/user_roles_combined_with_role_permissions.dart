import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/permissions_state.dart';
import 'package:whatever/bloc/user_and_role_permissions_combined_bloc.dart';
import 'package:whatever/bloc/user_and_role_permissions_combined_event.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/dashboard/responsive.dart';
import 'components/user_and_role_permissions_combined_files.dart';

class UserPermissionsCombinedWithRolePermissions extends StatefulWidget {
  final int userId;
  final String userUrl;
  final int page = 1;

  const UserPermissionsCombinedWithRolePermissions({
    Key key,
    this.userId,
    this.userUrl,
  }) : super(key: key);

  @override
  _UserPermissionsCombinedWithRolePermissionsState createState() =>
      _UserPermissionsCombinedWithRolePermissionsState();
}

class _UserPermissionsCombinedWithRolePermissionsState
    extends State<UserPermissionsCombinedWithRolePermissions> {
  List<PermissionsModel> permissions;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void initState() {
    super.initState();

    BlocProvider.of<UserAndRolePermissionsCombinedBloc>(context).add(
        UserPermissionsCombinedWithRolePermissionsRequested(
            userUrl: widget.userUrl, context: ''));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            SizedBox(height: defaultPadding),
            BlocBuilder<PermissionsBloc, PermissionsState>(
                builder: (context, state) {
              if (state is PermissionsLoadInProgress) {
                return CircularProgressIndicator();
              }
              if (state is PermissionsLoadSuccess) {
                permissions = state.permissions;
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          SizedBox(height: defaultPadding),
                          UserAndRolePermissionsCombinedFiles(
                            subTitle0: 'Permission Name',
                            subTitle1: 'Description',
                            subTitle2: 'Permissions ID',
                            subTitle3: 'Delete User',
                            dataString: 'Permissions',
                            title: 'Users',
                            length: state.permissions.length,
                            permissions: state.permissions,
                          ),
                          SizedBox(height: 20),
                          if (Responsive.isMobile(context))
                            SizedBox(height: defaultPadding),
                        ],
                      ),
                    ),
                    if (!Responsive.isMobile(context))
                      SizedBox(width: defaultPadding),
                  ],
                );
              }
              if (state is PermissionsLoadFailure) {
                return Container(
                  child: Text('Something went wrong'),
                  height: 30,
                  width: 40,
                  color: Colors.red,
                );
              }

              return Container(
                height: 20,
              );
            }),
          ],
        ),
      ),
    );
  }
}
