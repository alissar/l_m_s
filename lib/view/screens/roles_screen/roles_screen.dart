import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/roles_bloc.dart';
import 'package:whatever/bloc/roles_event.dart';
import 'package:whatever/bloc/roles_state.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/dashboard/responsive.dart';
import 'components/roles_files.dart';
import 'components/storage_details.dart';

class RolesScreen extends StatefulWidget {
  const RolesScreen({Key key}) : super(key: key);

  @override
  _RolesScreenState createState() => _RolesScreenState();
}

class _RolesScreenState extends State<RolesScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void initState() {
    super.initState();

    BlocProvider.of<RolesBloc>(context).add(AllRolesRequested());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            SizedBox(height: defaultPadding),
            BlocBuilder<RolesBloc, RolesState>(
              builder: (context, state) {
                if (state is RolesLoadInProgress) {
                  return CircularProgressIndicator();
                }
                if (state is RolesLoadSuccess) {
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 5,
                        child: Column(
                          children: [
                            SizedBox(height: defaultPadding),
                            RolesFiles(
                              length: state.roles.length,
                              roles: state.roles,
                            ),
                            SizedBox(height: 20),
                            if (Responsive.isMobile(context))
                              SizedBox(height: defaultPadding),
                            if (Responsive.isMobile(context)) StarageDetails(),
                          ],
                        ),
                      ),
                      if (!Responsive.isMobile(context))
                        SizedBox(width: defaultPadding),
                    ],
                  );
                }
                if (state is RolesLoadFailure) {
                  return Container(
                    child: Text('Something went wrong'),
                    height: 30,
                    width: 40,
                    color: Colors.red,
                  );
                }

                return Container(
                  height: 20,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
