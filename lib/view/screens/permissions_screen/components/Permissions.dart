import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';

class PermissionsFiles extends StatefulWidget {
  final List<PermissionsModel> permissions;
  final int length;
  final String title;
  final String dataString;
  final String subTitle0;
  final String subTitle1;
  final String subTitle2;
  final String subTitle3;
  final bool deletedUSers;
  const PermissionsFiles({
    Key key,
    this.permissions,
    this.length,
    this.title,
    this.dataString,
    this.subTitle0,
    this.subTitle1,
    this.subTitle2,
    this.subTitle3,
    this.deletedUSers,
  }) : super(key: key);

  @override
  _PermissionsFilesState createState() => _PermissionsFilesState();
}

class _PermissionsFilesState extends State<PermissionsFiles> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        padding: EdgeInsets.all(defaultPadding),
        decoration: BoxDecoration(
          color: DefultColors.PRIMARY_DARK_COLOR,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text(widget.title,
                  style: TextStyle(color: DefultColors.LIGHT_COLOR)),
            ),
            SizedBox(height: 28),
            SizedBox(
              width: double.infinity,
              child: DataTable2(
                columnSpacing: defaultPadding,
                minWidth: 600,
                columns: [
                  DataColumn(
                    label: Text(
                      widget.subTitle0,
                      style: TextStyle(color: DefultColors.LIGHT_COLOR),
                    ),
                  ),
                  DataColumn(
                    label: Text(widget.subTitle1,
                        style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                  ),
                  DataColumn(
                    label: Text(widget.subTitle2,
                        style: TextStyle(color: DefultColors.LIGHT_COLOR)),
                  ),
                ],
                rows: List.generate(
                  widget.length,
                  (index) => userDataRow(widget.permissions[index], context,
                      widget.dataString, widget.deletedUSers),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

DataRow userDataRow(PermissionsModel permissionModel, context,
    String dataString, bool deletedUSers) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Text(
                permissionModel.perName,
                style: TextStyle(color: DefultColors.LIGHT_COLOR),
              ),
            ),
          ],
        ),
      ),
      DataCell(
        Text(
          permissionModel.perDescription,
          style: TextStyle(color: DefultColors.LIGHT_COLOR),
        ),
      ),
      DataCell(
        InkWell(
          child: Text(
            '        ' + permissionModel.id.toString(),
            style: TextStyle(color: DefultColors.LIGHT_COLOR),
          ),
        ),
      ),
    ],
  );
}
