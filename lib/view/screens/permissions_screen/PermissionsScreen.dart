import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/permissions_event.dart';
import 'package:whatever/bloc/permissions_state.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/model/permissions_model.dart';
import 'package:whatever/view/screens/dashboard/constants.dart';
import 'package:whatever/view/screens/dashboard/responsive.dart';
import 'components/Permissions.dart';
import 'components/storage_details.dart';

class PermissionsScreen extends StatefulWidget {
  final int id;
  final int page = 1;

  const PermissionsScreen({Key key, this.id}) : super(key: key);

  @override
  _PermissionsScreenState createState() => _PermissionsScreenState();
}

class _PermissionsScreenState extends State<PermissionsScreen> {
  List<PermissionsModel> permissions;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void initState() {
    super.initState();

    BlocProvider.of<PermissionsBloc>(context).add(AllPermissionsRequested());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            SizedBox(height: defaultPadding),
            BlocBuilder<PermissionsBloc, PermissionsState>(
                builder: (context, state) {
              if (state is PermissionsLoadInProgress) {
                return CircularProgressIndicator();
              }
              if (state is PermissionsLoadSuccess) {
                permissions = state.permissions;
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          SizedBox(height: defaultPadding),
                          PermissionsFiles(
                            subTitle0: 'Permission Name',
                            subTitle1: 'Description',
                            subTitle2: 'Permissions ID',
                            subTitle3: 'Delete User',
                            dataString: 'Permissions',
                            title: 'Users',
                            length: state.permissions.length,
                            permissions: state.permissions,
                          ),
                          SizedBox(height: 20),
                          if (Responsive.isMobile(context))
                            SizedBox(height: defaultPadding),
                          if (Responsive.isMobile(context)) StarageDetails(),
                        ],
                      ),
                    ),
                    if (!Responsive.isMobile(context))
                      SizedBox(width: defaultPadding),
                  ],
                );
              }
              if (state is PermissionsLoadFailure) {
                return Container(
                  child: Text('Something went wrong'),
                  height: 30,
                  width: 40,
                  color: Colors.red,
                );
              }

              return Container(
                height: 20,
              );
            }),
          ],
        ),
      ),
    );
  }
}
