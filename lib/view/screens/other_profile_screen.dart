import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/edit_profile_bloc.dart';
import 'package:whatever/bloc/edit_profile_event.dart';
import 'package:whatever/bloc/edit_profile_state.dart';
import 'package:whatever/bloc/other_profile_bloc.dart';
import 'package:whatever/bloc/other_profile_event.dart';
import 'package:whatever/bloc/other_profile_state.dart';
import 'package:whatever/bloc/profile_bloc.dart';
import 'package:whatever/bloc/profile_events.dart';
import 'package:whatever/bloc/profile_states.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/screens/reset_password_screen.dart';
import 'package:whatever/model/user_model.dart';
import 'package:whatever/view/widgets/layout/responsive_flex.dart';
import 'package:whatever/view/widgets/profile_screen_section.dart';
import 'package:whatever/view/widgets/titled_text_feild_with_border.dart';

class OtherProfileScreen extends StatefulWidget {
  const OtherProfileScreen({Key key, this.url}) : super(key: key);

  @override
  _OtherProfileScreenState createState() => _OtherProfileScreenState();
  final String url;
}

class _OtherProfileScreenState extends State<OtherProfileScreen> {
  TextEditingController firstNameEditingController = TextEditingController();
  TextEditingController middleNameEditingController = TextEditingController();
  TextEditingController lastNameEditingController = TextEditingController();
  TextEditingController nicknameEditingController = TextEditingController();
  TextEditingController emailEditingController = TextEditingController();
  TextEditingController phoneEditingController = TextEditingController();
  TextEditingController phone2EditingController = TextEditingController();
  TextEditingController addressEditingController = TextEditingController();
  TextEditingController cityEditingController = TextEditingController();
  TextEditingController instEditingController = TextEditingController();
  TextEditingController depEditingController = TextEditingController();
  TextEditingController countryTextEditingController = TextEditingController();

  bool _editable = false;
  String _lable = 'Edit Profile';

  void fillControllers(UserModel user) {
    firstNameEditingController.text = user.firstName;
    middleNameEditingController.text = user.middleName;
    lastNameEditingController.text = user.lastName;
    nicknameEditingController.text = user.nickname;
    emailEditingController.text = user.email;
    phoneEditingController.text = user.phone;
    phone2EditingController.text = user.phone2;
    addressEditingController.text = user.address;
    cityEditingController.text = user.city;
    instEditingController.text = user.institution;
    depEditingController.text = user.department;
    countryTextEditingController.text = user.country;
  }

  @override
  void initState() {
    BlocProvider.of<OtherProfileBloc>(context)
        .add(OtherProfileRequested(url: widget.url));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double widthSize = MediaQuery.of(context).size.width;
    final double heightSize = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: BlocBuilder<OtherProfileBloc, OtherProfileState>(
            builder: (context, state) {
          if (state is OtherProfileLoadSuccess) {
            fillControllers(state.user);
            return Column(
              children: [
                Container(
                  height: heightSize * 0.5,
                  child: Stack(children: [
                    ResponsiveFlex(
                      direction: Axis.vertical,
                      children: [
                        Container(
                          width: widthSize,
                          height: heightSize * 0.4,
                          child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage('assets/images/profile.png'),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          alignment: Alignment.topRight,
                          padding: EdgeInsets.all(10),
                          child: BlocBuilder<EditProfileBloc, EditProfileState>(
                            builder: (context, state) {
                              if (state is EditProfileInitial) {
                                return ElevatedButton.icon(
                                  label: Text(_lable),
                                  icon: Icon(Icons.edit),
                                  style: ElevatedButton.styleFrom(
                                      elevation: 20,
                                      shadowColor: Colors.grey,
                                      primary: DefultColors.PRIMARY_DARK_COLOR),
                                  onPressed: () {
                                    setState(() {
                                      if (_editable == false) {
                                        _editable = true;
                                        _lable = 'Submit';
                                      } else {
                                        BlocProvider.of<EditProfileBloc>(
                                                context)
                                            .add(EditProfileRequested(
                                          userId: null,
                                          firstName:
                                              firstNameEditingController.text,
                                          lastName:
                                              lastNameEditingController.text,
                                          middleName:
                                              middleNameEditingController.text,
                                          nickname:
                                              nicknameEditingController.text,
                                          email: emailEditingController.text,
                                          city: cityEditingController.text,
                                          country:
                                              countryTextEditingController.text,
                                          department: depEditingController.text,
                                          address:
                                              addressEditingController.text,
                                          institution:
                                              instEditingController.text,
                                          imagePath: null,
                                          lang: null,
                                          theme: null,
                                          url: null,
                                        ));
                                        print('event added');
                                      }
                                    });
                                  },
                                );
                              }
                              if (state is EditProfileInProgress) {
                                return CircularProgressIndicator();
                              }
                              if (state is EditProfileLoadSuccess) {
                                _editable = false;
                                _lable = 'Edit Profile';
                                BlocProvider.of<EditProfileBloc>(context)
                                    .add(Reset());
                                BlocProvider.of<ProfileBloc>(context)
                                    .add(ProfileRequested());
                                return Container();
                              }
                              if (state is EditProfileLoadFailure) {
                                return Center(
                                  child: Text(state.error),
                                );
                              }
                              return Container();
                            },
                          ),
                        ),
                        Container(
                          alignment: Alignment.topRight,
                          padding: EdgeInsets.all(10),
                          child: ElevatedButton.icon(
                            label: Text('Reset password'),
                            icon: Icon(Icons.edit),
                            style: ElevatedButton.styleFrom(
                                elevation: 20,
                                shadowColor: Colors.grey,
                                primary: DefultColors.PRIMARY_DARK_COLOR),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ResetPasswordScreen()));
                            },
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      top: 180,
                      left: 100,
                      width: 600,
                      child: Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: DefultColors.PRIMARY_DARK_COLOR,
                            radius: 70.0,
                            child: CircleAvatar(
                              radius: 60.0,
                              backgroundColor: Colors.white,
                              foregroundImage:
                                  AssetImage('assets/images/signIn.jpg'),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  state.user.firstName +
                                      state.user.middleName +
                                      state.user.lastName,
                                  style: TextStyle(
                                      fontSize: DefultFontSize.MEDIUM_FONT_SIZE,
                                      fontWeight: DefultFontWeight.BOLD,
                                      color: Colors.white,
                                      fontFamily: 'Roboto'),
                                ),
                                Text(
                                  state.user.email,
                                  style: TextStyle(
                                      fontSize:
                                          DefultFontSize.VERY_SMALL_FONT_SIZE,
                                      fontWeight: DefultFontWeight.THIN,
                                      color: Colors.white,
                                      fontFamily: 'Roboto'),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
                Container(
                    child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ProfileScreenSection(
                              name: 'Personal :',
                              fields: [
                                TitledTextFeildWithBordersClass(
                                  editable: _editable,
                                  controller: firstNameEditingController,
                                  obsecureText: false,
                                  title: ('Name'),
                                ),
                                TitledTextFeildWithBordersClass(
                                  editable: _editable,
                                  controller: lastNameEditingController,
                                  obsecureText: false,
                                  title: 'Nick Name',
                                ),
                                TitledTextFeildWithBordersClass(
                                  editable: _editable,
                                  controller: emailEditingController,
                                  obsecureText: false,
                                  title: 'Email',
                                ),
                              ],
                            ),
                            ProfileScreenSection(
                              name: 'Other :',
                              fields: [
                                TitledTextFeildWithBordersClass(
                                  editable: _editable,
                                  controller: depEditingController,
                                  obsecureText: false,
                                  title: 'Department',
                                ),
                                TitledTextFeildWithBordersClass(
                                  editable: _editable,
                                  controller: instEditingController,
                                  obsecureText: false,
                                  title: 'Institution',
                                ),
                                TitledTextFeildWithBordersClass(
                                  editable: _editable,
                                  obsecureText: false,
                                  title: 'Theme',
                                ),
                              ],
                            ),
                          ]),
                    ),
                    Expanded(
                        child: Column(
                      children: [
                        ProfileScreenSection(
                          name: 'Contact :',
                          fields: [
                            TitledTextFeildWithBordersClass(
                              editable: _editable,
                              controller: addressEditingController,
                              obsecureText: false,
                              title: 'Address:',
                            ),
                            TitledTextFeildWithBordersClass(
                              editable: _editable,
                              controller: cityEditingController,
                              obsecureText: false,
                              title: 'City',
                            ),
                            TitledTextFeildWithBordersClass(
                              editable: _editable,
                              controller: phoneEditingController,
                              obsecureText: false,
                              title: 'Phone:',
                            ),
                            TitledTextFeildWithBordersClass(
                              editable: _editable,
                              controller: phone2EditingController,
                              obsecureText: false,
                              title: 'Phone 2:',
                            ),
                          ],
                        ),
                        ProfileScreenSection(
                          name: 'Courses :',
                          fields: [
                            TitledTextFeildWithBordersClass(
                              editable: _editable,
                              obsecureText: false,
                              title: 'English',
                            ),
                            TitledTextFeildWithBordersClass(
                              editable: _editable,
                              obsecureText: false,
                              title: 'Arabic',
                            ),
                            TitledTextFeildWithBordersClass(
                              editable: _editable,
                              obsecureText: false,
                              title: 'Scince',
                            ),
                          ],
                        ),
                      ],
                    ))
                  ],
                )),
              ],
            );
          } else if (state is OtherProfileLoadInProgress) {
            return CircularProgressIndicator();
          } else if (state is OtherProfileLoadFailure) {
            return Text('We fucked up. Sorry, I guess');
          } else {
            return Text('Initial');
          }
        }),
      ),
    );
  }
}
