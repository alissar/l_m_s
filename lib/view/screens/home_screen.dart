import 'package:flutter/material.dart';
import 'package:whatever/view/screens/landing_page.dart';
import 'package:whatever/view/widgets/navigation_bar_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column( 
          children: [
            NavigationBarWidget(),
            LandingPage(),
          ],
        ),
      ),
    );
  }
}
