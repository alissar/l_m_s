import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatever/common/settings.dart';
import 'package:whatever/view/widgets/navbar_button_widget.dart';

class LandingScreenClass extends StatefulWidget {
  @override
  _LandingScreenClassState createState() => _LandingScreenClassState();
}

class _LandingScreenClassState extends State<LandingScreenClass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: DefultColors.LIGHT_COLOR,
        leading: TextButton(
          child: Text(
            'L_M_S',
            style: TextStyle(
                color: DefultColors.SECONDARY_DARK_COLOR,
                fontWeight: DefultFontWeight.BOLD),
          ),
          onPressed: () {},
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            NavbarButtonWidget(
              text: 'home',
            ),
            NavbarButtonWidget(
              text: 'home',
            ),
            NavbarButtonWidget(
              text: 'home',
            ),
            NavbarButtonWidget(
              text: 'home',
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.9,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/landingFBG.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(450, 50, 0, 0),
                    child: Text(
                      "learning management System",
                      style: TextStyle(
                          color: DefultColors.LIGHT_COLOR,
                          fontSize: DefultFontSize.Very_LARGE_FONT_SIZE),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(450, 0, 0, 0),
                    child: Text(
                      "learning management System",
                      style: TextStyle(
                          color: DefultColors.LIGHT_COLOR,
                          fontSize: DefultFontSize.SMALL_FONT_SIZE),
                    ),
                  ),
                ],
              ),
              Positioned(
                top: 600,
                height: 200,
                child: Container(
                  decoration: BoxDecoration(color: DefultColors.LIGHT_COLOR),
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: Container(
                          child: Text(
                            'at has been delayed also to have some privacy ',
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: Container(
                          child: Text(
                              'at has been delayed also to have some privacy '),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: Container(
                          child: Text(
                              'hat has been delayed also to have some privacy '),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Text('hfhs;fs;kfjj'),
          )
        ]),
      ),
    );
  }
}
