class UserModel {
  final int id;
  final String firstName;
  final String middleName;
  final String lastName;
  final String nickname;
  final String email;
  final String phone;
  final String phone2;
  final String address;
  final String institution;
  final String department;
  final String city;
  final String country;
  final String token;
  final String url;

  UserModel(
      {this.id,
      this.firstName,
      this.middleName,
      this.lastName,
      this.nickname,
      this.email,
      this.phone,
      this.address,
      this.token,
      this.phone2,
      this.institution,
      this.department,
      this.city,
      this.country,
      this.url});

  factory UserModel.fromJson(Map<String, dynamic> data) {
    // TODO make sure these values conform with JSON received from server
    return UserModel(
        id: data['id'],
        firstName: data['firstName'],
        middleName: data['fatherName'],
        lastName: data['lastName'],
        nickname: data['nickName'],
        email: data['email'],
        phone: data['phone'],
        address: data['address'],
        token: data['token'],
        phone2: data['phone2'],
        institution: data['institution'],
        department: data['department'],
        city: data['city'],
        country: data['country'],
        url: data['url']);
  }
}
