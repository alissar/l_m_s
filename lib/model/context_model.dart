class ContextModel {
  final int id;
  final String course;
  final int sourceId;
  ContextModel({this.id, this.course, this.sourceId});

  static ContextModel fromJson(Map<String, dynamic> json) {
    return ContextModel(
        id: json['id'], course: json['course'], sourceId: json['sourceId']);
  }
}
