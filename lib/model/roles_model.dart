import 'package:whatever/model/permissions_model.dart';

class RolesModel {
  final int order;
  final int id;
  final String name;
  final String description;
  final List<dynamic> permissions;

  RolesModel(
      {this.order, this.name, this.description, this.permissions, this.id});

  static RolesModel fromJson(Map<String, dynamic> json) {
    return RolesModel(
      order: json['r_order'],
      name: json['name'],
      description: json['description'],
      permissions: json['permissions'],
      id: json['id'],
    );
  }
}
