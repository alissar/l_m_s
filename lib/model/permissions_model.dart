class PermissionsModel {
  final int id;
  final String perName;
  final String perDescription;

  PermissionsModel({
    this.perName,
    this.perDescription,
    this.id,
  });

  static PermissionsModel fromJson(Map<String, dynamic> json) {
    return PermissionsModel(
      id: json['id'],
      perName: json['name'],
      perDescription: json['description'],
    );
  }
}
