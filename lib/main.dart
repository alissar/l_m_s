import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whatever/bloc/authentication_bloc.dart';
import 'package:whatever/bloc/authentication_state.dart';
import 'package:whatever/bloc/edit_profile_bloc.dart';
import 'package:whatever/bloc/other_profile_bloc.dart';
import 'package:whatever/bloc/permissions_bloc.dart';
import 'package:whatever/bloc/profile_bloc.dart';
import 'package:whatever/bloc/reset_password_bloc.dart';
import 'package:whatever/bloc/reset_password_req_bloc.dart';
import 'package:whatever/bloc/role_bloc.dart';
import 'package:whatever/bloc/roles_bloc.dart';
import 'package:whatever/bloc/sign_in_bloc.dart';
import 'package:whatever/bloc/sign_up_bloc.dart';
import 'package:whatever/bloc/user_bloc.dart';
import 'package:whatever/bloc/user_permissions_bloc.dart';
import 'package:whatever/common/user_preferences.dart';
import 'package:whatever/view/screens/create_role_screen.dart';
import 'package:whatever/view/screens/landing_page.dart';
import 'package:whatever/view/screens/profile_screen.dart';
import 'package:whatever/view/screens/role_permissions_screen/role_permissions_screen.dart';
import 'package:whatever/view/screens/sign_in_screen.dart';
import 'package:whatever/view/screens/sign_up_screen.dart';
import 'package:whatever/view/screens/user_Permissions_screen/UserPermissionsScreen.dart';
import 'package:whatever/view/screens/roles_screen/roles_screen.dart';
import 'package:whatever/view/screens/user_permissions_combined_with_role_permissions/user_roles_combined_with_role_permissions.dart';
import 'package:whatever/view/screens/user_roles_screen/user_role_screen.dart';
import 'package:whatever/view/screens/users_screen/users_screen.dart';

void main() {
  UserPreferences().unstoreCookie();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<OtherProfileBloc>(
          create: (context) => OtherProfileBloc(),
        ),
        BlocProvider<RolesBloc>(
          create: (context) => RolesBloc(),
        ),
        BlocProvider<RoleBloc>(
          create: (context) => RoleBloc(),
        ),
        BlocProvider<PermissionsBloc>(
          create: (context) => PermissionsBloc(),
        ),
        BlocProvider<UserPermissionsBloc>(
          create: (context) => UserPermissionsBloc(),
        ),
        BlocProvider<RolesBloc>(
          create: (context) => RolesBloc(),
        ),
        BlocProvider<ProfileBloc>(
          create: (context) => ProfileBloc(),
        ),
        BlocProvider<EditProfileBloc>(
          create: (context) => EditProfileBloc(),
        ),
        BlocProvider<UserBloc>(
          create: (context) => UserBloc(),
        ),
        BlocProvider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(),
        ),
        BlocProvider<SignInBloc>(
          create: (context) => SignInBloc(
            authBloc: BlocProvider.of<AuthenticationBloc>(context),
          ),
        ),
        BlocProvider<SignUpBloc>(
          create: (context) => SignUpBloc(),
        ),
        BlocProvider<ResetPasswordReqBloc>(
          create: (context) => ResetPasswordReqBloc(),
        ),
        BlocProvider<ResetPasswordBloc>(
          create: (context) => ResetPasswordBloc(),
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is Unauthenticated) {
            return LandingPage();
          }
          if (state is Authenticated) {
            return ProfileScreen();
          }
          if (state is AuthenticationInProgress) {
            return CircularProgressIndicator();
          }
          return Container();
        },
      ),
    );
  }
}
